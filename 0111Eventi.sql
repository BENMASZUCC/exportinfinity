/* PRINCIPALE = SOGGETTI-EVENTI */




/*****************************************************************************************************************/
/************************************************* SOGGETTI-TIPO_EVENTI ******************************************/
/*****************************************************************************************************************/

;select 
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'TIPO_EVENTO'			     										  AS 'Tabella di riferimento',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)					  AS 'Codice',
Evento		    													  AS 'Descrizione'
from EBC_Eventi

/*****************************************************************************************************************/
/************************************************* SOGGETTI-TIPO_RICHIESTA ***************************************/
/*****************************************************************************************************************/

;select 
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'TIPO_RICHIESTA'		     										  AS 'Tabella di riferimento',
'0000000001'														  AS 'Codice',
'ALTRO'																  AS 'Descrizione'

/*****************************************************************************************************************/
/************************************************* SOGGETTI-STATUS_RICHIESTA *************************************/
/*****************************************************************************************************************/

;select 
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'STATUS_RICHIESTA'		     										  AS 'Tabella di riferimento',
'0000000001'														  AS 'Codice',
'ACCETTATA IMMEDIATAMENTE'											  AS 'Descrizione'
union
select 
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'STATUS_RICHIESTA'		     										  AS 'Tabella di riferimento',
'0000000002'														  AS 'Codice',
'ACCETTATA A PARTIRE DAL'											  AS 'Descrizione'
union
select 
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'STATUS_RICHIESTA'		     										  AS 'Tabella di riferimento',
'0000000003'														  AS 'Codice',
'RIFIUTATA'															  AS 'Descrizione'
union
select 
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'STATUS_RICHIESTA'		     										  AS 'Tabella di riferimento',
'0000000004'														  AS 'Codice',
'IN ESAME'															  AS 'Descrizione'
union
select 
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'STATUS_RICHIESTA'		     										  AS 'Tabella di riferimento',
'0000000005'														  AS 'Codice',
'DA VALUTARE DOPO IL'												  AS 'Descrizione'


/*****************************************************************************************************************/
/************************************************* SOGGETTI-RICHIESTE ***********************************************/
/*****************************************************************************************************************/

-- finire

;select 
'B27'																  AS 'Tipo record',
'A'																	  AS 'Modalità operativa',
'000000'		     												  AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), a.ID),16)			  AS 'Codice soggetto',
FORMAT(rd.Data,'dd/MM/yyyy')										  AS 'Data richiesta',
''																	  AS 'Data scadenza richiesta',
'0000000001'														  AS 'Tipo richiesta',
CASE
    WHEN rd.stato = 'ACCETTATA IMMEDIATAMENTE'THEN '0000000001'
    WHEN rd.stato = 'ACCETTATA A PARTIRE DAL' THEN '0000000002'
    WHEN rd.stato = 'RIFIUTATA'	THEN '0000000003'
	WHEN rd.stato = 'IN ESAME' THEN '0000000004'
	WHEN rd.stato = 'DA VALUTARE DOPO IL' THEN '0000000005'
    ELSE ''
END																	  AS 'Status richiesta',
RI.Richiesta														  AS 'Motivo',
''																	  AS 'Note',
''																	  AS 'Valuta',
''																	  AS 'Importo'
from RichiesteDip rd
left join RichiesteInterne RI on RI.id=rd.IDRichiesta
left join anagrafica a on a.id=rd.IDParteDaChi



/*****************************************************************************************************************/
/************************************************* SOGGETTI-EVENTI ***********************************************/
/*****************************************************************************************************************/

;SELECT
'B28'																  AS 'Tipo record',
'A'																	  AS 'Modalità operativa',
'000000'		     												  AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), IDAnagrafica),16)	  AS 'Codice soggetto',
FORMAT(DataEvento,'dd/MM/yyyy')										  AS 'Data inizio',
FORMAT(DataEvento,'dd/MM/yyyy')										  AS 'Data fine',
RIGHT('0000000000' + CONVERT(varchar(10), e.ID),10)					  AS 'Tipo evento',
--'0000000001'														  AS 'Tipo richiesta',
--FORMAT(DataEvento,'dd/MM/yyyy')	     								  AS 'Data richiesta',
''																	  AS 'Tipo richiesta',
''																	  AS 'Data richiesta',
s.annotazioni														  AS 'Note'
from Storico s
  inner join EBC_Eventi e on e.id = s.IDEvento



