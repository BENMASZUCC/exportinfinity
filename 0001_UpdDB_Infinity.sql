/* ATTENZIONE: puntare sul DB in produzione */ 

/*****************************************************************************************************/
/******************************************* INFINITY_STATI ******************************************/
/*****************************************************************************************************/

/*
select * from EBC_Stati
left join EBC_TipiStato on EBC_TipiStato.ID = EBC_Stati.IDTipoStato
*/

IF OBJECT_ID('dbo.INFINITY_STATI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_STATI;
GO

/*CREAZIONE TABELLA STATI*/
create table INFINITY_STATI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    ID_H1 int NOT NULL,
	DESCRIZIONE_H1 Varchar(40),
	IDTIPO_H1 int,
	TIPODESCRIZIONE_H1 Varchar(40),
	STATO_INFINITY_ID varchar(5),
	STATO_INFINITY_DES Varchar(100),
)

delete from INFINITY_STATI

INSERT INTO INFINITY_STATI
           (ID_H1,DESCRIZIONE_H1,IDTIPO_H1,TIPODESCRIZIONE_H1,STATO_INFINITY_ID,STATO_INFINITY_DES)
SELECT
EBC_Stati.Id Id_H1,
Stato Stato_H1,
IdTipoStato IdTipoStato_H1,
TipoStato TipoStato_H1,
CASE WHEN Stato IN (
		 'LICENZIATO',
		 '(Licenziato) IN EVIDENZA',
		 '(Licenziato) IN VALUTAZIONE',
		 '(Licenziato) IN ASSUNZIONE'
		 ) THEN	1
	WHEN Stato IN ('in Aspettativa') THEN 3
	WHEN Stato IN ( 'CONSULENTE ATTIVO',
		 'CONSULENTE NON ATTIVO',
		 'CONSULENTE ACCREDITATO',
		 'CONSULENTE IN VALUTAZIONE'
		 ) THEN 11
	WHEN Stato IN ('DIMESSO') THEN 7
	WHEN Stato IN ('CESSATO',
		 '(Cessato) IN EVIDENZA',
		 '(Cessato) IN VALUTAZIONE',
		 '(Cessato) IN ASSUNZIONE'
		 ) THEN 10
	WHEN Stato IN ('(Uscito) IN EVIDENZA',
		 '(Uscito) IN VALUTAZIONE',
		 '(Uscito) IN ASSUNZIONE',
		 'USCITO'
		 ) THEN	13
	WHEN Stato IN ('PENSIONATO') THEN 99
	ELSE NULL
END
AS STATI_INFINITY_ID,


CASE WHEN Stato IN (
		 'LICENZIATO',
		 '(Licenziato) IN EVIDENZA',
		 '(Licenziato) IN VALUTAZIONE',
		 '(Licenziato) IN ASSUNZIONE'
		 ) THEN	'Licenziato o sospeso'
	WHEN Stato IN ('in Aspettativa') THEN 'In aspettativa senza assegni'
	WHEN Stato IN ( 'CONSULENTE ATTIVO',
		 'CONSULENTE NON ATTIVO',
		 'CONSULENTE ACCREDITATO',
		 'CONSULENTE IN VALUTAZIONE'
		 ) THEN 'Professionista'
	WHEN Stato IN ('DIMESSO') THEN 'Dimissioni (no giusta causa)'
	WHEN Stato IN ('CESSATO',
		 '(Cessato) IN EVIDENZA',
		 '(Cessato) IN VALUTAZIONE',
		 '(Cessato) IN ASSUNZIONE'
		 ) THEN 'Non in forza per cessato incarico'
	WHEN Stato IN ('(Uscito) IN EVIDENZA',
		 '(Uscito) IN VALUTAZIONE',
		 '(Uscito) IN ASSUNZIONE',
		 'USCITO'
		 ) THEN	'Altre cause'
	WHEN Stato IN ('PENSIONATO') THEN 'In pensionamento'
	ELSE NULL
END
AS STATI_INFINITY_DES

FROM EBC_Stati
left join EBC_TipiStato on EBC_TipiStato.ID = EBC_Stati.IDTipoStato
--WHERE IdTipoStato NOT IN (1,2,3,5,6,9,11,15,17) -- da verificare: ci sono idtipstato dove � presente una categoria da escludere ed una da includere
WHERE EBC_Stati.Id NOT IN (27,28,29,30,31,32,33,35,38,39,40,46,51,52,53,57,58,61,63,64,66,67,68,72,73)


/*
SELECT * FROM INFINITY_STATI
ORDER BY STATO_INFINITY_DES
*/


/*****************************************************************************************************/
/******************************************* INFINITY_RUOLI ******************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_RUOLI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_RUOLI;
GO

create table INFINITY_RUOLI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(100)
)

INSERT INTO INFINITY_RUOLI (Descrizione)  SELECT DISTINCT substring(m.Descrizione + ' (' +  o.descrizione + ')',0,100) as des from Mansioni m left join Organigramma o on o.IDMansione=m.ID where m.descrizione is not null and o.descrizione is not null order by des


/*****************************************************************************************************/
/******************************************* INFINITY_MANSIONI****************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_MANSIONI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_MANSIONI;
GO

create table INFINITY_MANSIONI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(100)
)

INSERT INTO INFINITY_MANSIONI (Descrizione)  SELECT DISTINCT substring(m.Descrizione,0,100) as des from Mansioni m where m.descrizione is not null order by des

/*****************************************************************************************************/
/************************************* INFINITY_PIANOFORMAZIONE **************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_PIANOFORMAZIONE', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_PIANOFORMAZIONE;
GO

create table INFINITY_PIANOFORMAZIONE
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(100)
)

INSERT INTO INFINITY_PIANOFORMAZIONE
           (Descrizione)
SELECT DISTINCT DATEPART(YEAR,Previsionale_DataInizio)  from Form_AttivitaEdizioni WHERE Previsionale_DataInizio IS NOT NULL
GO


/*****************************************************************************************************/
/************************************* INFINITY_LIVELLICONTRATTI *************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_TIPOLOGIACORSI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_TIPOLOGIACORSI;
GO

create table INFINITY_TIPOLOGIACORSI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(100)
)

INSERT INTO INFINITY_TIPOLOGIACORSI
           (Descrizione)
select DISTINCT Tipologia from CorsiExtra  where Tipologia is not null
GO


/*****************************************************************************************************/
/************************************* INFINITY_LIVELLICONTRATTI *************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_LIVELLICONTRATTI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_LIVELLICONTRATTI;
GO

create table INFINITY_LIVELLICONTRATTI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
	Contratto int,
    Descrizione Varchar(100)
)

INSERT INTO INFINITY_LIVELLICONTRATTI
           (Contratto, Descrizione)
select IDContrattoNaz,Qualifica from QualificheContrattuali
GO




/*****************************************************************************************************/
/************************************* INFINITY_TIPOLOGIAFORMATORI ***************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_TIPOLOGIAFORMATORI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_TIPOLOGIAFORMATORI;
GO

create table INFINITY_TIPOLOGIAFORMATORI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(100)
)

INSERT INTO INFINITY_TIPOLOGIAFORMATORI
           (Descrizione)
SELECT DISTINCT S.Stato FROM Form_Formatori FF INNER JOIN ANAGRAFICA A ON A.ID = FF.IDAnagrafica INNER JOIN EBC_STATI S ON S.ID = A.IDStato
GO



/*****************************************************************************************************/
/************************************* INFINITY_AULECORSI ***************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_AULECORSI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_AULECORSI;
GO

create table INFINITY_AULECORSI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(100),
	indirizzo Varchar(100),
	referente Varchar(100)
)

INSERT INTO INFINITY_AULECORSI
           (fa.Descrizione
		   ,l.indirizzo
		   ,l.referente)
SELECT distinct LuogoSvolgimento, Indirizzo, Referente FROM Form_Attivita fa
 join Form_Luoghi l on fa.LuogoSvolgimento=l.Descrizione
 where LuogoSvolgimento is not null AND LuogoSvolgimento<>''
GO


--select * from INFINITY_AULECORSI



/*****************************************************************************************************/
/************************************* INFINITY_TIPOLOGIACORSI ***************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_TIPOLOGIACORSI', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_TIPOLOGIACORSI;
GO

create table INFINITY_TIPOLOGIACORSI
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(100)
)

INSERT INTO INFINITY_TIPOLOGIACORSI
           (Descrizione)
SELECT DISTINCT TIPOLOGIA FROM CorsiExtra WHERE TIPOLOGIA IS NOT NULL
GO



/*****************************************************************************************************/
/*************************************** INFINITY_MILITARE ****************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_MILITARE', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_MILITARE;
GO

create table INFINITY_MILITARE
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    Descrizione Varchar(15)
)

INSERT INTO INFINITY_MILITARE
           (Descrizione)
SELECT distinct ServizioMilitareStato FROM AnagAltreInfo where ServizioMilitareStato is not null
GO




/*****************************************************************************************************/
/************************************** INFINITY_TIPOCOMPETENZE **************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_TIPOCOMPETENZE', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_TIPOCOMPETENZE;
GO


-- CREAZIONE TABELLA [dbo].[INFINITY_TIPOCOMPETENZE]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].INFINITY_TIPOCOMPETENZE(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descrizione] [varchar](800) NOT NULL
CONSTRAINT [PK_INFINITY_TIPOCOMPETENZE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO INFINITY_TIPOCOMPETENZE
           (Descrizione)
select distinct Tipologia from Competenze where Tipologia is not null and Tipologia<>''
GO

/*****************************************************************************************************/
/*************************************** INFINITY_NAZIONALITA ****************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_NAZIONALITA', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_NAZIONALITA;
GO

create table INFINITY_NAZIONALITA
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    DescNazionalita Varchar(40),
	[CNT] [int]
)


/*****************************************************************************************************/
/************************************* INFINITY_ANAGCATPROTETTE **************************************/
/*****************************************************************************************************/

--select * from catprotette
--select * from INFINITY_ANAGCATPROTETTE


IF OBJECT_ID('dbo.INFINITY_ANAGCATPROTETTE', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_ANAGCATPROTETTE;
GO

/*CREAZIONE TABELLA CATEGORIE PROTETTE*/
/*CON DUE RECORD ISCRITTO-NON ISCRITTO*/
create table INFINITY_ANAGCATPROTETTE
(
    [ID] [int] IDENTITY(1,1) NOT NULL,
    DESCRIZIONE Varchar(40),
	CATPROT_INFINITY_ID varchar(5),
	CATPROT_INFINITY_DES Varchar(100),
	INVALIDITA_INFINITY_ID Varchar(5),
	INVALIDITA_INFINITY_DES Varchar(100)
)

delete from INFINITY_ANAGCATPROTETTE

INSERT INTO INFINITY_ANAGCATPROTETTE
           (DESCRIZIONE,CATPROT_INFINITY_ID,CATPROT_INFINITY_DES,INVALIDITA_INFINITY_ID,INVALIDITA_INFINITY_DES)
select
Descrizione CatProtette_H1,
CASE Descrizione
	WHEN	'Cieco assoluto'					THEN '01C'
	WHEN	'Cieco parziale'					THEN '01C'
	WHEN	'Invalido civile di guerra'			THEN '01D'
	WHEN	'Invalido civile parziale'			THEN 'IC'
	WHEN	'Invalido civile totale'			THEN 'IC'
	WHEN	'Invalido del lavoro'				THEN '01B'
	WHEN	'Invalido del personale militare'	THEN '01D'
	WHEN	'Invalido della protezione civile'	THEN '01D'
	WHEN	'Invalido di guerra'				THEN '01D'
	WHEN	'Invalido per servizio'				THEN '01D'
	WHEN	'Orfano del lavoro'					THEN '180'
	WHEN	'Orfano di guerra'					THEN '180'
	WHEN	'Orfano per servizio'				THEN '180'
	WHEN	'Sordomuto'							THEN '01C'
	WHEN	'Vedova del lavoro'					THEN '180'
	WHEN	'Vedova di guerra'					THEN '180'
	WHEN	'Vedova per servizio'				THEN '180'
	WHEN	'Vittima del terrorismo'			THEN '180'
	WHEN	'Profugo'							THEN '180'
	ELSE NULL
END
AS CATPROT_INFINITY_ID,

CASE Descrizione
	WHEN	'Cieco assoluto'					THEN 'Non vedenti/Sordomuti-L.68/1999 a.1/c'
	WHEN	'Cieco parziale'					THEN 'Non vedenti/Sordomuti-L.68/1999 a.1/c'
	WHEN	'Invalido civile di guerra'			THEN 'Invalidi guerra/Serv.-L.68/1999 a.1/d'
	WHEN	'Invalido civile parziale'			THEN 'Invalido civile'
	WHEN	'Invalido civile totale'			THEN 'Invalido civile'
	WHEN	'Invalido del lavoro'				THEN 'Invalidi del lavoro-L.68/1999 a.1/b'
	WHEN	'Invalido del personale militare'	THEN 'Invalidi guerra/Serv.-L.68/1999 a.1/d'
	WHEN	'Invalido della protezione civile'	THEN 'Invalidi guerra/Serv.-L.68/1999 a.1/d'
	WHEN	'Invalido di guerra'				THEN 'Invalidi guerra/Serv.-L.68/1999 a.1/d'
	WHEN	'Invalido per servizio'				THEN 'Invalidi guerra/Serv.-L.68/1999 a.1/d'
	WHEN	'Orfano del lavoro'					THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	WHEN	'Orfano di guerra'					THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	WHEN	'Orfano per servizio'				THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	WHEN	'Sordomuto'							THEN 'Non vedenti/Sordomuti-L.68/1999 a.1/c'
	WHEN	'Vedova del lavoro'					THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	WHEN	'Vedova di guerra'					THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	WHEN	'Vedova per servizio'				THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	WHEN	'Vittima del terrorismo'			THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	WHEN	'Profugo'							THEN 'Orfani/Vedove/Profughi-L.68/1999 a.18/2'
	ELSE NULL
END
AS CATPROT_INFINITY_DES,

CASE Descrizione
	WHEN	'Cieco assoluto'					THEN NULL
	WHEN	'Cieco parziale'					THEN NULL
	WHEN	'Invalido civile di guerra'			THEN 'Z002'
	WHEN	'Invalido civile parziale'			THEN 'Z001'
	WHEN	'Invalido civile totale'			THEN 'Z001'
	WHEN	'Invalido del lavoro'				THEN 'Z004'
	WHEN	'Invalido del personale militare'	THEN 'Z002'
	WHEN	'Invalido della protezione civile'	THEN 'Z003'
	WHEN	'Invalido di guerra'				THEN 'Z002'
	WHEN	'Invalido per servizio'				THEN 'Z003'
	WHEN	'Orfano del lavoro'					THEN NULL
	WHEN	'Orfano di guerra'					THEN NULL
	WHEN	'Orfano per servizio'				THEN NULL
	WHEN	'Sordomuto'							THEN NULL
	WHEN	'Vedova del lavoro'					THEN NULL
	WHEN	'Vedova di guerra'					THEN NULL
	WHEN	'Vedova per servizio'				THEN NULL
	WHEN	'Vittima del terrorismo'			THEN NULL
	WHEN	'Profugo'							THEN NULL
	ELSE NULL
END
AS INVALIDITA_INFINITY_ID,

CASE Descrizione
	WHEN	'Cieco assoluto'					THEN NULL
	WHEN	'Cieco parziale'					THEN NULL
	WHEN	'Invalido civile di guerra'			THEN 'INVALIDI DI GUERRA (D.P.R. 915/78)'
	WHEN	'Invalido civile parziale'			THEN 'INVALIDI CIVILI (LEGGI 66/62,381/70,382/70,118/71)'
	WHEN	'Invalido civile totale'			THEN 'INVALIDI CIVILI (LEGGI 66/62,381/70,382/70,118/71)'
	WHEN	'Invalido del lavoro'				THEN 'INVALIDI DEL LAVORO (D.P.R. 1124/64)'
	WHEN	'Invalido del personale militare'	THEN 'INVALIDI DI GUERRA (D.P.R. 915/78)'
	WHEN	'Invalido della protezione civile'	THEN 'INVALIDI PER SERVIZIO (D.P.R. 915/78)'
	WHEN	'Invalido di guerra'				THEN 'INVALIDI DI GUERRA (D.P.R. 915/78)'
	WHEN	'Invalido per servizio'				THEN 'INVALIDI PER SERVIZIO (D.P.R. 915/78)'
	WHEN	'Orfano del lavoro'					THEN NULL
	WHEN	'Orfano di guerra'					THEN NULL
	WHEN	'Orfano per servizio'				THEN NULL
	WHEN	'Sordomuto'							THEN NULL
	WHEN	'Vedova del lavoro'					THEN NULL
	WHEN	'Vedova di guerra'					THEN NULL
	WHEN	'Vedova per servizio'				THEN NULL
	WHEN	'Vittima del terrorismo'			THEN NULL
	WHEN	'Profugo'							THEN NULL
	ELSE NULL
END
AS INVALIDITA_INFINITY_DES

FROM CatProtette





/*****************************************************************************************************/
/************************************ INFINITY_LIVELLOTECNOLOGIA *************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_LIVELLOTECNOLOGIA', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_LIVELLOTECNOLOGIA;
GO


-- CREAZIONE TABELLA [dbo].[INFINITY_LIVELLOTECNOLOGIA]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].INFINITY_LIVELLOTECNOLOGIA(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descrizione] [varchar](800) NOT NULL
CONSTRAINT [PK_INFINITY_LIVELLOTECNOLOGIA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- INSERIMENTO NELLA TABELLA [dbo].[INFINITY_LIVELLOTECNOLOGIA] DEI VALORI "Livello" RECUPERATI DALLA TABELLA "AnagConoscInfo"

truncate table INFINITY_LIVELLOTECNOLOGIA

INSERT INTO INFINITY_LIVELLOTECNOLOGIA
	SELECT DISTINCT Livello
	FROM AnagConoscInfo
	WHERE
		Livello IS NOT NULL or
		Livello <> ''
	ORDER BY Livello

/*****************************************************************************************************/
/************************************ INFINITY_TIPOTECNOLOGIA ****************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_TIPOTECNOLOGIA', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_TIPOTECNOLOGIA;
GO


-- CREAZIONE TABELLA [dbo].[[INFINITY_TIPOTECNOLOGIA]]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[INFINITY_TIPOTECNOLOGIA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descrizione] [varchar](800) NOT NULL
CONSTRAINT [PK_INFINITY_TIPOTECNOLOGIA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

TRUNCATE TABLE INFINITY_TIPOTECNOLOGIA

INSERT INTO INFINITY_TIPOTECNOLOGIA
	SELECT DISTINCT Software
	FROM AnagConoscInfo
	WHERE
		Software IS NOT NULL AND
		Software <> ''
	ORDER BY Software

/*****************************************************************************************************/
/************************************ INFINITY_TIPOTITOLIDISTUDIO*************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_TIPOTITOLIDISTUDIO', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_TIPOTITOLIDISTUDIO;
GO


-- CREAZIONE TABELLA [dbo].[[INFINITY_TIPOTITOLIDISTUDIO]]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].INFINITY_TIPOTITOLIDISTUDIO(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descrizione] [varchar](60) NOT NULL,
	[TipologiaStandard] [varchar](1) NOT NULL
CONSTRAINT [PK_INFINITY_TIPOTITOLIDISTUDIO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

TRUNCATE TABLE [dbo].[INFINITY_TIPOTITOLIDISTUDIO]

INSERT INTO [dbo].[INFINITY_TIPOTITOLIDISTUDIO]
           ([Descrizione]
           ,[TipologiaStandard])
SELECT Tipo,
CASE Tipo
    WHEN 'DIPLOMA ' THEN 'D'
    WHEN 'LAUREA ' THEN 'L'
    ELSE 'A'
END
 FROM DIPLOMI
GROUP BY Tipo
GO

/*****************************************************************************************************/
/****************************************** INFINITY_LINGUE*******************************************/
/*****************************************************************************************************/

IF OBJECT_ID('dbo.INFINITY_LINGUE', 'U') IS NOT NULL
DROP TABLE dbo.INFINITY_LINGUE;
GO


-- CREAZIONE TABELLA [dbo].[INFINITY_LINGUE]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].INFINITY_LINGUE(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descrizione] [varchar](60) NOT NULL
CONSTRAINT [PK_INFINITY_LINGUE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

TRUNCATE TABLE [dbo].[INFINITY_LINGUE]

INSERT INTO [dbo].[INFINITY_LINGUE]
           ([Descrizione])
SELECT Lingua
FROM Lingue
GO

/*****************************************************************************************************/
/*************************************** FUNCTION RTF2Text *******************************************/
/*****************************************************************************************************/

IF EXISTS (SELECT * FROM sysobjects WHERE name = 'RTF2Text' AND xtype IN ('FN', 'IF', 'TF'))

	DROP FUNCTION RTF2Text

GO

CREATE FUNCTION [dbo].[RTF2Text]
(
    @rtf nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
    DECLARE @Pos1 int;
    DECLARE @Pos2 int;
    DECLARE @hex varchar(316);
    DECLARE @Stage table
    (
        [Char] char(1),
        [Pos] int
    );

    INSERT @Stage
        (
           [Char]
         , [Pos]
        )
    SELECT SUBSTRING(@rtf, [Number], 1)
         , [Number]
      FROM [master]..[spt_values]
     WHERE ([Type] = 'p')
       AND (SUBSTRING(@rtf, Number, 1) IN ('{', '}'));

    SELECT @Pos1 = MIN([Pos])
         , @Pos2 = MAX([Pos])
      FROM @Stage;

    DELETE
      FROM @Stage
     WHERE ([Pos] IN (@Pos1, @Pos2));

    WHILE (1 = 1)
        BEGIN
            SELECT TOP 1 @Pos1 = s1.[Pos]
                 , @Pos2 = s2.[Pos]
              FROM @Stage s1
                INNER JOIN @Stage s2 ON s2.[Pos] > s1.[Pos]
             WHERE (s1.[Char] = '{')
               AND (s2.[Char] = '}')
            ORDER BY s2.[Pos] - s1.[Pos];

            IF @@ROWCOUNT = 0
                BREAK

            DELETE
              FROM @Stage
             WHERE ([Pos] IN (@Pos1, @Pos2));

            UPDATE @Stage
               SET [Pos] = [Pos] - @Pos2 + @Pos1 - 1
             WHERE ([Pos] > @Pos2);

            SET @rtf = STUFF(@rtf, @Pos1, @Pos2 - @Pos1 + 1, '');
        END

    SET @rtf = REPLACE(@rtf, '\pard', '');
    SET @rtf = REPLACE(@rtf, '\par', '');
    SET @rtf = STUFF(@rtf, 1, CHARINDEX(' ', @rtf), '');

    WHILE (Right(@rtf, 1) IN (' ', CHAR(13), CHAR(10), '}'))
      BEGIN
        SELECT @rtf = SUBSTRING(@rtf, 1, (LEN(@rtf + 'x') - 2));
        IF LEN(@rtf) = 0 BREAK
      END
    
    SET @Pos1 = CHARINDEX('\''', @rtf);

    WHILE @Pos1 > 0
        BEGIN
            IF @Pos1 > 0
                BEGIN
                    SET @hex = '0x' + SUBSTRING(@rtf, @Pos1 + 2, 2);
                    SET @rtf = REPLACE(@rtf, SUBSTRING(@rtf, @Pos1, 4), CHAR(CONVERT(int, CONVERT (binary(1), @hex,1))));
                    SET @Pos1 = CHARINDEX('\''', @rtf);
                END
        END

    SET @rtf = @rtf + ' ';

    SET @Pos1 = PATINDEX('%\%[0123456789][\ ]%', @rtf);

    WHILE @Pos1 > 0
        BEGIN
            SET @Pos2 = CHARINDEX(' ', @rtf, @Pos1 + 1);

            IF @Pos2 < @Pos1
                SET @Pos2 = CHARINDEX('\', @rtf, @Pos1 + 1);

            IF @Pos2 < @Pos1
                BEGIN
                    SET @rtf = SUBSTRING(@rtf, 1, @Pos1 - 1);
                    SET @Pos1 = 0;
                END
            ELSE
                BEGIN
                    SET @rtf = STUFF(@rtf, @Pos1, @Pos2 - @Pos1 + 1, '');
                    SET @Pos1 = PATINDEX('%\%[0123456789][\ ]%', @rtf);
                END
        END

    /*DEVO GESTIRE IL CASO DI UNA STRIGA VUOTA CONVERTITA IN RTF ALTRIMENTI DAREBBE ERRORE*/
	IF @rtf <> ' '
	BEGIN
		IF RIGHT(@rtf, 1) = ' '
		 BEGIN
          SET @rtf = SUBSTRING(@rtf, 1, LEN(@rtf) -1);
		 END
	END
	ELSE
	BEGIN
	    SET @rtf = ''
	END

    RETURN isnull(@rtf, '');
END
GO





-- AGGIORNAMENTO VERSIONI

update Global set verH1hrms='7.1.0'
GO
update Global set verH1Sel='5.13.0'
GO
update Global set versione='---'
GO

if exists (select name from sysobjects where name = 'SAC_Global')
BEGIN
update SAC_Global set VersioneH1Sel = VerH1Sel From Global
update SAC_Global set VersioneH1HRMS = VerH1HRMS From Global
update SAC_Global set VersioneQW = '1.0.0' 
END 


--------------------------------------------------------------------------------
-- ### non aggiungere niente sotto !! ###



