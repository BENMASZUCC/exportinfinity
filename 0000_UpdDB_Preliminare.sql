
/****** Object:  Table [dbo].[Infinity_ConfigMigrazione]    Script Date: 11/10/2019 09:21:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Infinity_ConfigMigrazione](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Enabled] [int] NULL,
	[StringaConnOrigine] [varchar](500) NULL,
	[NomeTabOrigine] [varchar](500) NULL,
	[CampoChiaveAnag] [varchar](500) NULL,
	[CampoValore] [varchar](500) NULL,
	[CodPagheFile] [varchar](500) NULL,
	[CodPagheCampo] [varchar](500) NULL,
	[Tipo] [varchar](1) NULL,
	[QueryFile] [varchar](max) NULL,
	[QueryRicavaAnag] [text] NULL,
	[Filtro] [varchar](50) NULL,
	[Ordine] [int] NULL,
	[ElencoColonneRetr] [varchar](500) NULL,
	[DirFileMigrazione] [varchar](255) NULL,
	[DisabilitaCreazioneAnagrafica] [bit] NULL,
	[DisabilitaCreazioneRetributivi] [bit] NULL,
	[ImportDatiPresenzeMesePrecedente] [bit] NULL,
	[InserisciStatoAnag] [int] NULL,
	[InserisciTipoStatoAnag] [int] NULL,
	[SoftwarePaghe] [varchar](200) NULL,
	[PacchettoFile] [varchar](2000) NULL,
	[AggiornaStatoUpdateAnag] [bit] NULL,
	[CodCampiMappa] [varchar](50) NULL,
	[CodAziendaRetribColonna] [bit] NULL,
	[ParsaFile00] [bit] NULL,
	[RicavaQualificaLivello_NonUsareCampiDesc] [bit] NULL,
 CONSTRAINT [PK_Infinity_ConfigMigrazione] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF_Infinity_ConfigMigrazione_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF__Infinity___Disab__168E357B]  DEFAULT ((0)) FOR [DisabilitaCreazioneAnagrafica]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF__Infinity___Disab__178259B4]  DEFAULT ((0)) FOR [DisabilitaCreazioneRetributivi]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF__Infinity___Impor__18767DED]  DEFAULT ((0)) FOR [ImportDatiPresenzeMesePrecedente]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF__Infinity___Aggio__196AA226]  DEFAULT ((0)) FOR [AggiornaStatoUpdateAnag]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF__Infinity___CodAz__1A5EC65F]  DEFAULT ((0)) FOR [CodAziendaRetribColonna]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF__Infinity___Parsa__1B52EA98]  DEFAULT ((0)) FOR [ParsaFile00]
GO

ALTER TABLE [dbo].[Infinity_ConfigMigrazione] ADD  CONSTRAINT [DF__Infinity___Ricav__1C470ED1]  DEFAULT ((0)) FOR [RicavaQualificaLivello_NonUsareCampiDesc]
GO

/*
declare @nomeazienda varchar(50)
select @nomeazienda=nomeazienda from Global

if (lower(@nomeazienda) ='fraternitÓ sistemi')
begin
  delete AnagContattiAz where IDCampoContattoAz=4
  insert into AnagContattiAz select id,1 as azienda,4 as doc,Email from Anagrafica
  update Anagrafica set Email=CasellaPostale
  update anagrafica set CasellaPostale=null 
end

*/