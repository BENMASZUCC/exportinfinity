

-- ESTRAZIONE CERTIFICAZIONI CV



/*POPOLARE SU DB HR_ZUCC PRELIMINARIAMENTE:

enti certific atori (verifuicare se corretto l'equivalenza con la nostra cert_vendor)
ambito 0000000001 fisso
area (select * from Cert_TabAreaTecnica)
tecnologia  (select distinct IDAreaTecnica,idareafunzionale from Cert_Certificazioni where IDAreaTecnica is not null order by IDAreaTecnica)

*/

;select
'B50'                                                                      as 'TipoRecord',
'A'                                                                        as 'Modalità operativa',
'000000'																   as 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), Anagrafica.id),16)         as 'Codice soggetto',
ISNULL(FORMAT(AnagCertificazioni.DataConseguimento  ,'dd/MM/yyyy'),'')	   as 'Data validità',
ISNULL(FORMAT(AnagCertificazioni.DataScadenza,'dd/MM/yyyy'),'')		       as 'Data Scadenza',
COALESCE(Cert_Certificazioni.Descrizione,AltraCertificazione,'9999999999') as 'Titolo certificazione',
COALESCE(Cert_Vendors.ID,'')											   as 'Codice ente certificatore',  --codice cert_vendor
COALESCE(Cert_Vendors.Descrizione,'')                                      as 'Descrizione ente certificatore',  --descrizione cert_vendor   -- COPIARE AnagCertificazioni.AltroVendors IN TABELLA CERT_VENDOR
Cert_TabSigla.Descrizione                                                  as 'Numero certificazione',
'0000000001'                                                               as 'Ambito',  --0000000001
COALESCE(Cert_TabAreaTecnica.ID,'')                                        as 'Area',   --codice da  Cert_TabAreaTecnica
COALESCE(Cert_TabAreaFunzionale.ID,'')                                     as 'Tecnologia',  -- codice dm Cert_TabAreaFunzionale
''                                                                         as 'Versione certificato',
CONCAT(
	CASE
		WHEN Cert_TabLivelloVendors.Descrizione IS NOT NULL THEN
			'Livello ente certificatore: ' + Cert_TabLivelloVendors.Descrizione
		ELSE
			''
	END,
	CASE
		WHEN Cert_TabLivelloCertInterno.Descrizione IS NOT NULL THEN
			CASE
				WHEN Cert_TabLivelloVendors.Descrizione IS NOT NULL THEN
					', Livello di certificazione interno: ' + Cert_TabLivelloCertInterno.Descrizione
				ELSE
					'Livello di certificazione interno: ' + Cert_TabLivelloCertInterno.Descrizione
			END
		ELSE
			''
	END
	)
                                                                           as 'Competenze acquisite',
CASE
	WHEN Cert_Certificazioni.idTipolgia IS NOT NULL AND Cert_Certificazioni.idTipolgia = 1 THEN
		'S'
	ELSE
		'N'
END
                                                                           as 'Training con valore di certificazione',

'N'                                                                        as 'Valida per partnership',

CASE
	WHEN Cert_Certificazioni.idTipolgia IS NOT NULL AND Cert_Certificazioni.idTipolgia = 1 THEN
		'S'
	ELSE
		'N'
END
                                                                           as 'Certificato'

from AnagCertificazioni
left join Anagrafica                 on Anagrafica.id = AnagCertificazioni.idanagrafica
left join Cert_Certificazioni        on Cert_Certificazioni.ID = AnagCertificazioni.IDCertificazione
left join Cert_Vendors               on Cert_Vendors.ID = IDVendors
left join Cert_TabAreaTecnica        on Cert_TabAreaTecnica.ID = IDAreaTecnica
left join Cert_TabLivelloVendors     on Cert_TabLivelloVendors.ID = IDLivelloVendors
left join Cert_TabAreaFunzionale     on Cert_TabAreaFunzionale.ID = IDAreaFunzionale
left join Cert_TabLivelloCertInterno on Cert_TabLivelloCertInterno.ID = IDLivelloCertInterno
left join Cert_TabSigla              on Cert_TabSigla.ID = IDSigla
left join Cert_TabTipologia          on Cert_TabTipologia.ID = IDTipolgia