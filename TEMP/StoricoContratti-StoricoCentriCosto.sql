



WITH Storico AS (

select
'StoricoContratto' as TABELLASTORICO,
DataFineContr as DATASTORICO,
ID ID_StoricoContratto, 
IDAnagrafica IDAnagrafica_StoricoContratto,
codcontratto_OLD codcontratto_OLD_StoricoContratto,
TipoContratto_OLD TipoContratto_OLD_StoricoContratto,
DataFineContr DataFineContr_StoricoContratto,
categoria categoria_StoricoContratto,
livello livello_StoricoContratto,
PercPartTime PercPartTime_StoricoContratto,
IDContrattoNaz IDContrattoNaz_StoricoContratto,
IDCatProfess IDCatProfess_StoricoContratto,
IDCodCondContrattuale IDCodCondContrattuale_StoricoContratto,
IDQualifContr IDQualifContr_StoricoContratto,
IdStoricoMigrazione IdStoricoMigrazione_StoricoContratto,
CAST(Note AS NVARCHAR(MAX)) Note_StoricoContratto,
Cancellato Cancellato_StoricoContratto,
ContrattoNaz ContrattoNaz_StoricoContratto,
CatProfess CatProfess_StoricoContratto,
DescCondContrattuale DescCondContrattuale_StoricoContratto,
CodCondContrattuale CodCondContrattuale_StoricoContratto,
ScattiMaturati ScattiMaturati_StoricoContratto,
MensilitaAnnue MensilitaAnnue_StoricoContratto,
IDAzienda IDAzienda_StoricoContratto,
Azienda Azienda_StoricoContratto,
IDTipoPartTime IDTipoPartTime_StoricoContratto,
TipoPartTime TipoPartTime_StoricoContratto,
DataAssunzione DataAssunzione_StoricoContratto,
DataFineProva DataFineProva_StoricoContratto,
DataScadContratto DataScadContratto_StoricoContratto,
DataUltimaProm DataUltimaProm_StoricoContratto,
CategoriaPensione CategoriaPensione_StoricoContratto,
DataIngressoAzienda DataIngressoAzienda_StoricoContratto,
DataProroga DataProroga_StoricoContratto,
DataLicenziamento DataLicenziamento_StoricoContratto,
DataProssScatto DataProssScatto_StoricoContratto,
NumCertifPensione NumCertifPensione_StoricoContratto,
TempoIndet TempoIndet_StoricoContratto,
partitaiva partitaiva_StoricoContratto,
MeseTemp MeseTemp_StoricoContratto,
DataAssunzRicon DataAssunzRicon_StoricoContratto,
DataFineInserimento DataFineInserimento_StoricoContratto,
DataL104 DataL104_StoricoContratto,
L104_92 L104_92_StoricoContratto,
DataAssunzioneRicon_OLD DataAssunzioneRicon_OLD_StoricoContratto,
NULL ID_StoricoCentriCosto,
NULL IdAnagrafica_StoricoCentriCosto,
NULL IDAnagCC_StoricoCentriCosto,
NULL CentroCosto_StoricoCentriCosto,
NULL Grado_StoricoCentriCosto,
NULL Principale_StoricoCentriCosto,
NULL PercAttrib_StoricoCentriCosto,
NULL IDCCDivisione_StoricoCentriCosto,
NULL IDCCEnte_StoricoCentriCosto,
NULL IDCCGruppo_StoricoCentriCosto,
NULL IDCCFiliale_StoricoCentriCosto,
NULL IDCCReparto_StoricoCentriCosto,
NULL IDCCRagg1_StoricoCentriCosto,
NULL IDCCRagg2_StoricoCentriCosto,
NULL IDCCRagg3_StoricoCentriCosto,
NULL IDCCRagg4_StoricoCentriCosto,
NULL IDCCRagg5_StoricoCentriCosto,
NULL IDCCRagg6_StoricoCentriCosto,
NULL StringRagg_StoricoCentriCosto,
NULL Mese_StoricoCentriCosto,
NULL cancellato_StoricoCentriCosto,
NULL CodiceCC_StoricoCentriCosto,
NULL DescCC_StoricoCentriCosto,
NULL CodiceDivisione_StoricoCentriCosto,
NULL DescDivisione_StoricoCentriCosto,
NULL CodiceEnte_StoricoCentriCosto,
NULL DescEnte_StoricoCentriCosto,
NULL CodGruppo_StoricoCentriCosto,
NULL DescGruppo_StoricoCentriCosto,
NULL CodFiliale_StoricoCentriCosto,
NULL DescFiliale_StoricoCentriCosto,
NULL CodReparti_StoricoCentriCosto,
NULL DescReparti_StoricoCentriCosto,
NULL CodRagg1_StoricoCentriCosto,
NULL DescRagg1_StoricoCentriCosto,
NULL CodRagg2_StoricoCentriCosto,
NULL DescRagg2_StoricoCentriCosto,
NULL CodRagg3_StoricoCentriCosto,
NULL DescRagg3_StoricoCentriCosto,
NULL CodRagg4_StoricoCentriCosto,
NULL DescRagg4_StoricoCentriCosto,
NULL CodRagg5_StoricoCentriCosto,
NULL DescRagg5_StoricoCentriCosto,
NULL CodRagg6_StoricoCentriCosto,
NULL DescRagg6_StoricoCentriCosto,
NULL DescGrado_StoricoCentriCosto,
NULL Azienda_StoricoCentriCosto,
NULL IDAzienda_StoricoCentriCosto,
NULL IDCCMansione_StoricoCentriCosto,
NULL CodiceMansione_StoricoCentriCosto,
NULL DescMansione_StoricoCentriCosto,
NULL CodGrado_StoricoCentriCosto
from storicocontratto

union

select
'StoricoCentriCosto' as TABELLASTORICO,
Mese as DATASTORICO,
NULL ID_StoricoContratto, 
NULL IDAnagrafica_StoricoContratto,
NULL codcontratto_OLD_StoricoContratto,
NULL TipoContratto_OLD_StoricoContratto,
NULL DataFineContr_StoricoContratto,
NULL categoria_StoricoContratto,
NULL livello_StoricoContratto,
NULL PercPartTime_StoricoContratto,
NULL IDContrattoNaz_StoricoContratto,
NULL IDCatProfess_StoricoContratto,
NULL IDCodCondContrattuale_StoricoContratto,
NULL IDQualifContr_StoricoContratto,
NULL IdStoricoMigrazione_StoricoContratto,
NULL Note_StoricoContratto,
NULL Cancellato_StoricoContratto,
NULL ContrattoNaz_StoricoContratto,
NULL CatProfess_StoricoContratto,
NULL DescCondContrattuale_StoricoContratto,
NULL CodCondContrattuale_StoricoContratto,
NULL ScattiMaturati_StoricoContratto,
NULL MensilitaAnnue_StoricoContratto,
NULL IDAzienda_StoricoContratto,
NULL Azienda_StoricoContratto,
NULL IDTipoPartTime_StoricoContratto,
NULL TipoPartTime_StoricoContratto,
NULL DataAssunzione_StoricoContratto,
NULL DataFineProva_StoricoContratto,
NULL DataScadContratto_StoricoContratto,
NULL DataUltimaProm_StoricoContratto,
NULL CategoriaPensione_StoricoContratto,
NULL DataIngressoAzienda_StoricoContratto,
NULL DataProroga_StoricoContratto,
NULL DataLicenziamento_StoricoContratto,
NULL DataProssScatto_StoricoContratto,
NULL NumCertifPensione_StoricoContratto,
NULL TempoIndet_StoricoContratto,
NULL partitaiva_StoricoContratto,
NULL MeseTemp_StoricoContratto,
NULL DataAssunzRicon_StoricoContratto,
NULL DataFineInserimento_StoricoContratto,
NULL DataL104_StoricoContratto,
NULL L104_92_StoricoContratto,
NULL DataAssunzioneRicon_OLD_StoricoContratto,
Id ID_StoricoCentriCosto,
IDAnagrafica IdAnagrafica_StoricoCentriCosto,
IDAnagCC IDAnagCC_StoricoCentriCosto,
CentroCosto CentroCosto_StoricoCentriCosto,
Grado Grado_StoricoCentriCosto,
Principale Principale_StoricoCentriCosto,
PercAttrib PercAttrib_StoricoCentriCosto,
IDCCDivisione IDCCDivisione_StoricoCentriCosto,
IDCCEnte IDCCEnte_StoricoCentriCosto,
IDCCGruppo IDCCGruppo_StoricoCentriCosto,
IDCCFiliale IDCCFiliale_StoricoCentriCosto,
IDCCReparto IDCCReparto_StoricoCentriCosto,
IDCCRagg1 IDCCRagg1_StoricoCentriCosto,
IDCCRagg2 IDCCRagg2_StoricoCentriCosto,
IDCCRagg3 IDCCRagg3_StoricoCentriCosto,
IDCCRagg4 IDCCRagg4_StoricoCentriCosto,
IDCCRagg5 IDCCRagg5_StoricoCentriCosto,
IDCCRagg6 IDCCRagg6_StoricoCentriCosto,
StringRagg StringRagg_StoricoCentriCosto,
Mese Mese_StoricoCentriCosto,
cancellato cancellato_StoricoCentriCosto,
CodiceCC CodiceCC_StoricoCentriCosto,
DescCC DescCC_StoricoCentriCosto,
CodiceDivisione CodiceDivisione_StoricoCentriCosto,
DescDivisione DescDivisione_StoricoCentriCosto,
CodiceEnte CodiceEnte_StoricoCentriCosto,
DescEnte DescEnte_StoricoCentriCosto,
CodGruppo CodGruppo_StoricoCentriCosto,
DescGruppo DescGruppo_StoricoCentriCosto,
CodFiliale CodFiliale_StoricoCentriCosto,
DescFiliale DescFiliale_StoricoCentriCosto,
CodReparti CodReparti_StoricoCentriCosto,
DescReparti DescReparti_StoricoCentriCosto,
CodRagg1 CodRagg1_StoricoCentriCosto,
DescRagg1 DescRagg1_StoricoCentriCosto,
CodRagg2 CodRagg2_StoricoCentriCosto,
DescRagg2 DescRagg2_StoricoCentriCosto,
CodRagg3 CodRagg3_StoricoCentriCosto,
DescRagg3 DescRagg3_StoricoCentriCosto,
CodRagg4 CodRagg4_StoricoCentriCosto,
DescRagg4 DescRagg4_StoricoCentriCosto,
CodRagg5 CodRagg5_StoricoCentriCosto,
DescRagg5 DescRagg5_StoricoCentriCosto,
CodRagg6 CodRagg6_StoricoCentriCosto,
DescRagg6 DescRagg6_StoricoCentriCosto,
DescGrado DescGrado_StoricoCentriCosto,
Azienda Azienda_StoricoCentriCosto,
IDAzienda IDAzienda_StoricoCentriCosto,
IDCCMansione IDCCMansione_StoricoCentriCosto,
CodiceMansione CodiceMansione_StoricoCentriCosto,
DescMansione DescMansione_StoricoCentriCosto,
CodGrado CodGrado_StoricoCentriCosto
from storicocentricosto
)


select

CASE
	WHEN TABELLASTORICO ='StoricoCentriCosto' THEN
		(select top 1 S2.id_storicocontratto from Storico S2 where S2.TABELLASTORICO = 'StoricoContratto' AND S2.DATASTORICO >= S.DATASTORICO AND S2.IdAnagrafica_StoricoContratto = S.IdAnagrafica_StoricoCentriCosto)

	ELSE '0'
END as ID_StoricoContratto,

CASE
	WHEN TABELLASTORICO ='StoricoContratto' THEN
		(select top 1 S2.ID_StoricoCentriCosto from Storico S2 where S2.TABELLASTORICO = 'StoricoCentriCosto' AND S2.DATASTORICO >= S.DATASTORICO AND S2.IdAnagrafica_StoricoCentriCosto = S.IdAnagrafica_StoricoContratto)
	ELSE '0'
END as ID_StoricoCentriCosto,	


*
from Storico S
where IdAnagrafica_StoricoContratto = 60 OR IdAnagrafica_StoricoCentriCosto = 60
order by DATASTORICO




select * from StoricoContratto
where idanagrafica = 60


select *
from StoricoCentriCosto SCC
where idanagrafica =60

