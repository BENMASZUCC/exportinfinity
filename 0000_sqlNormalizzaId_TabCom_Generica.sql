use [Fer]
SET NOCOUNT ON;    
  
Declare	@ID int, @Codice varchar(4),@Descrizione varchar(100),@Prov varchar(2),@Cap varchar(10),@Prefisso varchar(4),@IDZona int,@Regione varchar(250),@AreaNielsen smallint,@IDRegione int,@CodiceIstat varchar(50),@ASL varchar(50)
  
DECLARE @CodiceRegione varchar(2)
DECLARE @CodiceUnit�territorialesovracomunale varchar(3)
DECLARE @CodiceProvincia varchar(3)
DECLARE @ProgressivodelComune varchar(3)
DECLARE @CodiceComuneformatoalfanumerico varchar(6)
DECLARE @DenominazioneItalianaestraniera varchar(100)
DECLARE @Denominazioneinitaliano varchar(100)
DECLARE @Denominazionealtralingua varchar(100)
DECLARE @CodiceRipartizioneGeografica varchar(2)
DECLARE @Ripartizionegeografica varchar(100)
DECLARE @Denominazioneregione varchar(100)
DECLARE @Denominazionuenit�territorialesovracomunale varchar(100)
DECLARE @FlagComunecapoluogodiprovincia varchar(5)
DECLARE @Siglaautomobilistica varchar(5)
DECLARE @CodiceComuneformatonumerico varchar(5)
DECLARE @CodiceComunenumericocon110provincedal2010al2016 varchar(5)
DECLARE @CodiceComunenumericocon107provincedal2006al2009 varchar(5)
DECLARE @CodiceComunenumericocon103provincedal1995al200 varchar(5)
DECLARE @CodiceCatastaledelcomune varchar(10)
DECLARE @Popolazionelegale201 varchar(50)
DECLARE @NUTS1 varchar(10)
DECLARE @NUTS23 varchar(10)
DECLARE @NUTS3 varchar(10)


/*Aggiunta campo Migrato (smallint) [0=non migrato, 1=migrato]*/
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('TabCom') and syscolumns.name = 'Migrato')
BEGIN
   alter table TabCom add Migrato smallint null
END



update TabCom set Codice='E844' where descrizione = 'altedo'

declare @ret1 int

  select @ret1=count(*) from anagrafica where Comune in ('XXXXXXXXXXXXXXX','AMSTERDAM','PROVA')

if @ret1>0
begin
 update anagrafica set Comune='' where Comune in ('XXXXXXXXXXXXXXX','AMSTERDAM','PROVA')
end

declare @ret2 int

select @ret2=count(*) from anagrafica where LuogoNascita in ('XXXXXXXXXXXXXXX','AMSTERDAM','PROVA')

if @ret2>0
begin
 update anagrafica set LuogoNascita='' where LuogoNascita in ('XXXXXXXXXXXXXXX','AMSTERDAM','PROVA')
end



Delete TabCom where Descrizione in ('XXXXXXXXXXXXXXX','AMSTERDAM','PROVA')


DECLARE comuni_cursor CURSOR FOR     
SELECT top 1000
[ID]
,[Codice]
,[Descrizione]
,[Prov]
,[Cap]
,[Prefisso]
,[IDZona]
,[Regione]
,[AreaNielsen]
,[IDRegione]
,[CodiceIstat]
,[ASL]
FROM TabCom 
where Migrato is null
order by ID;
  
OPEN comuni_cursor    
  
FETCH NEXT FROM comuni_cursor INTO @ID,@Codice,@Descrizione,@Prov,@Cap,@Prefisso,@IDZona,@Regione,@AreaNielsen,@IDRegione,@CodiceIstat,@ASL
  
print 'ID  Codice'       
  
WHILE @@FETCH_STATUS = 0    
BEGIN    

  Select  @CodiceRegione=[Codice Regione],
		  @CodiceUnit�territorialesovracomunale=[Codice dell'Unit� territoriale sovracomunale _(valida a fini sta],
		  @CodiceProvincia=[Codice Provincia (Storico)(1)],
		  @ProgressivodelComune=[Progressivo del Comune (2)],
		  @CodiceComuneformatoalfanumerico=[Codice Comune formato alfanumerico],
		  @DenominazioneItalianaestraniera=[Denominazione (Italiana e straniera)],
		  @Denominazioneinitaliano=[Denominazione in italiano],
		  @Denominazionealtralingua=[Denominazione altra lingua],
		  @CodiceRipartizioneGeografica=[Codice Ripartizione Geografica],
		  @Ripartizionegeografica=[Ripartizione geografica],
		  @Denominazioneregione=[Denominazione regione],
		  @Denominazionuenit�territorialesovracomunale=[Denominazione dell'Unit� territoriale sovracomunale _(valida a f],
		  @FlagComunecapoluogodiprovincia=[Flag Comune capoluogo di provincia/citt� metropolitana/libero co],
		  @Siglaautomobilistica=[Sigla automobilistica],
		  @CodiceComuneformatonumerico=[Codice Comune formato numerico],
		  @CodiceComunenumericocon110provincedal2010al2016=[Codice Comune numerico con 110 province (dal 2010 al 2016)],
		  @CodiceComunenumericocon107provincedal2006al2009=[Codice Comune numerico con 107 province (dal 2006 al 2009)],
		  @CodiceComunenumericocon103provincedal1995al200=[Codice Comune numerico con 103 province (dal 1995 al 2005)],
		  @CodiceCatastaledelcomune=[Codice Catastale del comune],
		  @Popolazionelegale201=[Popolazione legale 2011 (09/10/2011)],
		  @NUTS1=[NUTS1],
		  @NUTS23=[NUTS2(3) ],
		  @NUTS3=[NUTS3]
		FROM [CODICI COMUNI 20022019$] where [Codice Catastale del comune] = @Codice

    	select @Codice
		select @CodiceCatastaledelcomune

		if (@CodiceCatastaledelcomune!='')
		BEGIN
		   print 'OK   ' + CAST(@ID as varchar(5)) +'           '+  cast(@Codice as varchar(10))   +'           '+  cast(@Descrizione as varchar(100))

		   IF @Denominazioneregione = 'Trentino-Alto Adige/S�dtirol'
			BEGIN
				SET @Denominazioneregione='Trentino-Alto Adige'
			END
		   IF @Denominazioneregione = 'Valle d''Aosta/Vall�e d''Aoste'
			BEGIN
				SET @Denominazioneregione='Valle d''Aosta'
			END

			print 'OK1   '

			declare @CodComuneOLD varchar(4)
			declare @ProvComuneOLD varchar(2)
			declare @DescrizioneOLD varchar(100)
			
			/*Recupero codice e descrizione del comune vecchio*/
			select @CodComuneOLD=Codice, @ProvComuneOLD= Prov, @DescrizioneOLD=Descrizione FROM TabCom where Codice=@Codice

			update TabCom
            set
			Descrizione = @DenominazioneItalianaestraniera,
			Prov = @Siglaautomobilistica,
			--Cap = 
			--Prefisso=
			--[IDZona]=
			Regione=(select CodRegioneH1 from TabRegioni where Regione=@Denominazioneregione),
			AreaNielsen=(CASE
							WHEN @CodiceRipartizioneGeografica = 5 THEN 4
							ELSE @CodiceRipartizioneGeografica
						END),
			IDRegione = (select ID from TabRegioni where Regione=@Denominazioneregione),			
			CodiceIstat= @CodiceComuneformatonumerico,
			Migrato=1
			--[ASL]
			WHERE TabCom.Codice=@Codice

			print 'OK2   '
			
			--UPDATE Anagrafica SET comune= @DenominazioneItalianaestraniera, Provincia=@Siglaautomobilistica where comune = @DescrizioneOLD and Provincia=@ProvComuneOLD

		END
		else
		begin
		print 'NON OK   ' + CAST(@ID as varchar(5)) +'           '+  cast(@Codice as varchar(10))   +'           '+  cast(@Descrizione as varchar(100))
		end

    FETCH NEXT FROM comuni_cursor INTO @ID,@Codice,@Descrizione,@Prov,@Cap,@Prefisso,@IDZona,@Regione,@AreaNielsen,@IDRegione,@CodiceIstat,@ASL
   
END     
CLOSE comuni_cursor;    
DEALLOCATE comuni_cursor;  



Declare	@ID1 int, @Descrizione1 varchar(100)

DECLARE comuni_cursor CURSOR FOR     
SELECT
[ID],
[Descrizione]
FROM TabCom;
  
OPEN comuni_cursor    
  
FETCH NEXT FROM comuni_cursor INTO @ID1,@Descrizione1
WHILE @@FETCH_STATUS = 0    
BEGIN 

	 update Anagrafica
	 set Comune = @Descrizione1
	 where IDComuneRes=@ID1

    FETCH NEXT FROM comuni_cursor INTO @ID1,@Descrizione1
END
CLOSE comuni_cursor;
DEALLOCATE comuni_cursor;