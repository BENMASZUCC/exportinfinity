
/* PRINCIPALE : SOGGETTI */

/* COLLEGATE : STATI CIVILI - NAZIONALITA' - CATEGORIE PROTETTE - INVALIDITA */


/*****************************************************************************************************************************************/
/********************************************************** INVALIDITA *******************************************************************/
/*****************************************************************************************************************************************/

--Commento, si � deciso di non esportare le categorie di H1 ma di usare quelle di Infinity con un'opportuna mappatura
SELECT
'A02'                            AS 'Tipo record',
'C'                              AS 'Modalit� operativa',
'CAT_PROTETTE'                   AS 'Tabella di riferimento',
'001'							 AS 'Codice categoria protetta',
'Iscritto_h1'					 AS 'Descrizione categoria protetta'
/*
SELECT
'A02'                            AS 'Tipo record',
'C'                              AS 'Modalit� operativa',
'CAT_PROTETTE'                   AS 'Tabella di riferimento',
RIGHT('000' + CONVERT(varchar(3), ID),3) AS 'Codice categoria protetta',
CONVERT(varchar(40),Descrizione) + '_h1' AS 'Descrizione categoria protetta'
FROM INFINITY_ANAGCATPROTETTE
WHERE
	Descrizione IS NOT NULL AND
	Descrizione <> ''
ORDER BY ID
*/

/*****************************************************************************************************************************************/
/****************************************************** CATEGORIE PROTETTE ***************************************************************/
/*****************************************************************************************************************************************/

/*
SELECT
'A02'                            AS 'Tipo record',
'C'                              AS 'Modalit� operativa',
'CAT_PROTETTE'                   AS 'Tabella di riferimento',
CONVERT(varchar(3),ID)           AS 'Codice categoria protetta',
CONVERT(varchar(40),Descrizione) AS 'Descrizione categoria protetta'
FROM INFINITY_ANAGCATPROTETTE
ORDER BY ID
*/



/*****************************************************************************************************************************************/
/********************************************************* NAZIONALITA *******************************************************************/
/*****************************************************************************************************************************************/

truncate table INFINITY_NAZIONALITA

INSERT INTO INFINITY_NAZIONALITA
           (DescNazionalita)
select DescNazionalita from nazioni  where DescNazionalita IS NOT NULL order by DescNazionalita 


--select * from INFINITY_NAZIONALITA order by DescNazionalita

-------------------------------------------------------------------------------------------

DECLARE @ID int
DECLARE @DescNazionalita VARCHAR(256)
DECLARE @CNT int 
DECLARE @count int


DECLARE db_cursor CURSOR FOR 
select ID,DescNazionalita,CNT from INFINITY_NAZIONALITA order by DescNazionalita

OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO @ID,@DescNazionalita,@CNT

WHILE @@FETCH_STATUS = 0  
BEGIN  

	  SET @count=0

	  SELECT @count = COUNT(*) FROM INFINITY_NAZIONALITA WHERE DescNazionalita=@DescNazionalita

	  UPDATE INFINITY_NAZIONALITA SET CNT = @count WHERE ID=@ID

      FETCH NEXT FROM db_cursor INTO @ID,@DescNazionalita,@CNT
END 

CLOSE db_cursor  
DEALLOCATE db_cursor 

-------------------------------------------------------------------------------------------



;SELECT
'A02'												 AS 'Tipo record',
'C'													 AS 'Modalit� operativa',
'NAZIONALITA'										 AS 'Tabella di riferimento',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)	 AS 'Codice cittadinanza',
CASE
    WHEN CNT > 1 THEN CONVERT(varchar(60),DescNazionalita + ' (da H1)') + '_' + CONVERT(varchar(10), ISNULL(ID,0))
    ELSE CONVERT(varchar(60),DescNazionalita + ' (da H1)') 
END AS 'Descrizione cittadinanza'
FROM INFINITY_NAZIONALITA
WHERE DescNazionalita IS NOT NULL
ORDER BY ID, DescNazionalita


/*****************************************************************************************************************************************/
/*********************************************************STATI CIVILI********************************************************************/
/*****************************************************************************************************************************************/

;SELECT
'A02'                            AS 'Tipo record',
'C'                              AS 'Modalit� operativa',
'STATOCIVILE'                    AS 'Tabella di riferimento',
RIGHT('00' + CONVERT(varchar(2), ID),2) AS 'Codice stato civile',
CASE Descrizione
	WHEN CONVERT(varchar(60),'')                      THEN 'Non comunicato'
	ELSE CONVERT(varchar(60),Descrizione)
END AS 'Descrizione stato civile',
CONVERT(varchar(2),
CASE StatiCivili.Descrizione
	WHEN ''                      THEN '00' -- Non comunicato
	WHEN '-'                     THEN '00' -- Non comunicato
	WHEN 'stato civile 0'        THEN '00' -- Non comunicato
	WHEN 'No status'             THEN '00' -- Non comunicato
	WHEN '** non caricato **'    THEN '00' -- Non comunicato
	WHEN 'Celibe/Nubile'         THEN '01' -- Celibe/Nubile
	WHEN 'Nubile/Celibe'         THEN '01' -- Celibe/Nubile
	WHEN 'Nubile'                THEN '01' -- Celibe/Nubile
	WHEN 'libero'                THEN '01' -- Celibe/Nubile
	WHEN 'libera'                THEN '01' -- Celibe/Nubile
	WHEN 'libero/a'              THEN '01' -- Celibe/Nubile
	WHEN 'libero/libera'         THEN '01' -- Celibe/Nubile
	WHEN 'Single'                THEN '01' -- Celibe/Nubile
	WHEN 'Non coniugato'         THEN '02' -- Non coniugato
	WHEN 'convivente'            THEN '02' -- Non coniugato
	WHEN 'Coniugato/a'           THEN  ''  -- 
	WHEN 'Coniugata/o'           THEN  ''  -- 
	WHEN 'Coniugato'             THEN  ''  -- 
	WHEN 'Occupato/a'            THEN  ''  -- 
	WHEN 'Occupato'              THEN  ''  -- 
	WHEN 'Occupata'              THEN  ''  -- 
	WHEN 'Married'               THEN  ''  -- 
	WHEN 'Vedovo/a'              THEN '03' -- Vedovo/a
	WHEN 'Vedova/o'              THEN '03' -- Vedovo/a
	WHEN 'Vedovo'                THEN '03' -- Vedovo/a	
	WHEN 'Widowed'               THEN '03' -- Vedovo/a
	WHEN 'Separato/a'            THEN '04' -- Separato/a legalmente
	WHEN 'Separata/o'            THEN '04' -- Separato/a legalmente
	WHEN 'separato/a legalmente' THEN '04' -- Separato/a legalmente
	WHEN 'Separata/o'            THEN '04' -- Separato/a legalmente
	WHEN 'Separato'              THEN '04' -- Separato/a legalmente
	WHEN 'Separated'             THEN '04' -- Separato/a legalmente
	WHEN 'Divorziato/a'          THEN '05' -- Divorziato/a
	WHEN 'Divorziata/o'          THEN '05' -- Divorziato/a
	WHEN 'Divorziato'            THEN '05' -- Divorziato/a
	WHEN 'Divorced'              THEN '05' -- Divorziato/a
	WHEN 'Deceduto/a'            THEN '06' -- Deceduto/a
	WHEN 'Tutelato/a'            THEN '07' -- Tutelato/a
	WHEN 'Figlio/a minore'       THEN '08' -- Figlio/a minore
	ELSE '00'
END
)
AS 'Tipo stato civile'
FROM StatiCivili
--WHERE
--	Descrizione IS NOT NULL AND
--	Descrizione <> ''
ORDER BY ID




/*********************************************************************************************************/
/**************************************** SOGGETTI - ANAGRAFICA ******************************************/
/*********************************************************************************************************/
declare @Iscrittocatprot varchar(3)
select @Iscrittocatprot = RIGHT('000' + CONVERT(varchar(3), ID),3) from INFINITY_ANAGCATPROTETTE where descrizione = 'iscritto'
--select @Iscrittocatprot

declare @NonIscrittocatprot varchar(3)
select @NonIscrittocatprot = RIGHT('000' + CONVERT(varchar(3), ID),3) from INFINITY_ANAGCATPROTETTE where descrizione = 'non iscritto'
--select @NonIscrittocatprot

declare @StatoCivileNull varchar(2)
select @StatoCivileNull = RIGHT('00' + CONVERT(varchar(2), ID),2) FROM StatiCivili WHERE Descrizione = ''
--select @StatoCivileNull



;SELECT
'B01'																							 AS 'Tipo record',
'C' 																							 AS 'Modalit� operativa',
ISNULL(FORMAT(StoricoAnagrafica.DallaData,'dd/MM/yyyy'),'dd/MM/yyyy')							 AS 'Data validit�',
'000000'																					     AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), StoricoAnagrafica.IDAnagrafica),16)				 AS 'Codice soggetto',
CONVERT(varchar(40),UPPER(ISNULL(StoricoAnagrafica.Cognome,'')))								 AS 'Cognome',
CONVERT(varchar(40),UPPER(ISNULL(StoricoAnagrafica.Nome,'')))									 AS 'Nome',
CASE
	WHEN StoricoAnagrafica.Sesso IN ('M','F') THEN
		CONVERT(varchar(1),UPPER(ISNULL(StoricoAnagrafica.Sesso,'')))
	ELSE
		''
END																								 AS 'Sesso',
ISNULL(FORMAT(StoricoAnagrafica.DataNascita,'dd/MM/yyyy'),'')									 AS 'Data di nascita',
CONVERT(varchar(40),UPPER(ISNULL(StoricoAnagrafica.LuogoNascita,'')))							 AS 'Comune di nascita',
ISNULL(CONVERT(varchar(10),TabCom_Nascita.Cap),'')												 AS 'Cap di nascita',
CONVERT(varchar(10),UPPER(ISNULL(StoricoAnagrafica.ProvNascita,'')))							 AS 'Provincia di nascita',
CONVERT(varchar(10),UPPER(ISNULL(substring(StoricoAnagrafica.CodiceFiscale,12,4) ,'')))			 AS 'Codice catastale di nascita',
CASE
	
	WHEN UPPER(ISNULL(substring(StoricoAnagrafica.CodiceFiscale,12,1) ,'')) = 'Z' THEN
		(select Abbrev2 from nazioni where DescNazione=StoricoAnagrafica.LuogoNascita)
		--''
	ELSE
		'IT'
END				


																				 AS 'Stato di nascita',



CONVERT(varchar(16),UPPER(ISNULL(StoricoAnagrafica.CodiceFiscale,'')))							 AS 'Codice Fiscale',
''																							     AS 'Partita IVA',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(StoricoAnagrafica.Indirizzo,Anagrafica.Indirizzo))))) AS 'Indirizzo residenza',  
CONVERT(varchar(6),UPPER(ISNULL(StoricoAnagrafica.NumCivico,Anagrafica.NumCivico)))				 AS 'N� civico residenza',

CONVERT(varchar(40),UPPER(ISNULL(StoricoAnagrafica.Comune,StoricoAnagrafica.LuogoNascita)))					 AS 'Comune residenza',


CONVERT(varchar(10),ISNULL(StoricoAnagrafica.Cap,Anagrafica.Cap))								 AS 'Cap residenza',
CONVERT(varchar(10),UPPER(ISNULL(StoricoAnagrafica.Provincia,Anagrafica.Provincia)))			 AS 'Provincia residenza',
CONVERT(varchar(4),UPPER(ISNULL(TabCom_Residenza.Codice,'')))									 AS 'Codice catastale residenza',
CASE
	WHEN ISNULL(TabCom_Residenza.Codice,'') <> '' or (ISNULL(TabCom_Residenza.Prov,Anagrafica.Provincia) <> ''  or NOT ISNULL(TabCom_Residenza.Prov,Anagrafica.Provincia) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = Anagrafica.Stato),''))
	ELSE
		--''
		'IT'
END																								AS 'Stato residenza',
''																								AS 'Prefisso residenza',
CONVERT(varchar(20),ISNULL(StoricoAnagrafica.RecapitiTelefonici,''))							AS 'Telefono residenza',
CONVERT(varchar(20),ISNULL(StoricoAnagrafica.Fax,''))											AS 'Fax residenza',
CONVERT(varchar(20),ISNULL(StoricoAnagrafica.Cellulare,''))									    AS 'Cellulare',
CONVERT(varchar(70),LOWER(LTRIM(RTRIM(ISNULL(Anagrafica.Email,'')))))							AS 'E-mail',
''																								AS 'Indirizzo Web',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(StoricoAnagrafica.DomicilioIndirizzo,Anagrafica.DomicilioIndirizzo)))))		AS 'Indirizzo domicilio',  
CONVERT(varchar(6),UPPER(ISNULL(StoricoAnagrafica.DomicilioNumCivico,Anagrafica.DomicilioNumCivico)))						AS 'N� civico domicilio',
CONVERT(varchar(40),UPPER(ISNULL(TabCom_Domicilio.Descrizione,Anagrafica.DomicilioComune)))		AS 'Comune domicilio',
CONVERT(varchar(10),ISNULL(StoricoAnagrafica.DomicilioCap,Anagrafica.DomicilioCap))				AS 'Cap domicilio',
CONVERT(varchar(10),UPPER(ISNULL(StoricoAnagrafica.DomicilioProvincia,Anagrafica.DomicilioProvincia)))						AS 'Provincia domicilio',
CONVERT(varchar(4),UPPER(ISNULL(TabCom_Domicilio.Codice,'')))									AS 'Codice catastale domicilio',
CASE
	WHEN ISNULL(TabCom_Domicilio.Codice,'') <> '' or (ISNULL(TabCom_Domicilio.Prov,Anagrafica.DomicilioProvincia) <> ''  or NOT ISNULL(TabCom_Domicilio.Prov,Anagrafica.DomicilioProvincia) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = Anagrafica.DomicilioStato),''))
	ELSE
		''
END																								AS 'Stato domicilio',
''																								AS 'Prefisso domicilio',
''																								AS 'Telefono domicilio',
''																								AS 'Fax domicilio',
'DIPEND'																						AS 'Tipo soggetto',

CASE
	WHEN ISNULL(StoricoAnagrafica.IDStatoCivile,'') <> '' THEN
		RIGHT('00' + ISNULL(CONVERT(varchar(2), StoricoAnagrafica.IDStatoCivile),''),2)	
	ELSE
		@StatoCivileNull
END																								AS 'Stato civile',

CASE
	WHEN ISNULL(AnagAltreInfo.Nazionalita,'') <> '' THEN
		RIGHT('00' + CONVERT(varchar(2), Nazioni.ID),2)
	ELSE
		''
END																								AS 'Nazionalit�',


CASE
	WHEN AnagAltreInfo.invalidita IS NOT NULL THEN
		INFINITY_ANAGCATPROTETTE.CATPROT_INFINITY_ID
	ELSE
	
		CASE 
			WHEN AnagAltreInfo.CateogorieProtette = 'iscritto' /*  da verificare se inserire OR INFINITY_ANAGCATPROTETTE.CATPROT_INFINITY_ID */THEN
				'001'
			ELSE
				''
		END	
END
AS 'Categoria protetta',


CASE 
    WHEN AnagAltreInfo.CateogorieProtette = 'iscritto' /*  da verificare se inserire OR INFINITY_ANAGCATPROTETTE.CATPROT_INFINITY_ID */THEN
        'S'
    WHEN AnagAltreInfo.CateogorieProtette = 'non iscritto' THEN
        'N'
    ELSE
        ''
END    

AS 'Categoria protetta (S/N)',

REPLACE(CAST(AnagAltreInfo.Note AS NVARCHAR(MAX)), CHAR(13) + CHAR(10), '\n ')				    AS 'Note',
CASE 
    WHEN INFINITY_ANAGCATPROTETTE.INVALIDITA_INFINITY_ID IS NOT NULL OR (AnagAltreInfo.PercentualeInvalidita IS NOT NULL and AnagAltreInfo.PercentualeInvalidita > 0) THEN
        'S'
    ELSE
        ''
END

AS 'Invalidit� (S/N)',



INVALIDITA_INFINITY_ID
AS 'Codice invalidit�',

CASE 
    WHEN AnagAltreInfo.PercentualeInvalidita IS NOT NULL THEN
        CONVERT(decimal(6,2),AnagAltreInfo.PercentualeInvalidita)
    ELSE
        CONVERT(decimal(6,2),0)
END                                                                                                AS '% invalidit�',
CONVERT(varchar(40),UPPER(ISNULL(AnagAltreInfo.MedicoBase,'')))									AS 'Cognome medico di base',
''																								AS 'Nome medico di base',
CONVERT(varchar(40),UPPER(ISNULL(AnagAltreInfo.IndirizzoMedicoBase,'')))						AS 'Indirizzo medico di base',
''																								AS 'N. civico medico di base',
''																								AS 'Stato medico di base',
''																								AS 'Codice catastale medico di base',
''																								AS 'Citt� medico di base',
''																								AS 'Provincia medico di base',
''																								AS 'Cap medico di base',
''																								AS 'Prefisso medico di base',
CONVERT(varchar(20),ISNULL(AnagAltreInfo.TelContattoEmergSanit,''))								AS 'Telefono medico di base',
''																								AS 'Titolo onorifico',
--CONVERT(varchar(5),UPPER(ISNULL((select top 1 Abbrev2 FROM Nazioni WHERE Nazioni.DescNazionalita IS NOT NULL AND (UPPER(Nazioni.DescNazionalita) = UPPER(AnagAltreInfo.Nazionalita)) ),''))) AS 'Stato di cittadinanza',
CONVERT(varchar(5),UPPER(ISNULL((select top 1 Abbrev2 FROM Nazioni WHERE Nazioni.DescNazionalita IS NOT NULL AND (UPPER(Nazioni.DescNazionalita) = UPPER(AnagAltreInfo.Nazionalita)) ),'IT'))) AS 'Stato di cittadinanza',
''																								AS 'Stato di cittadinanza aggiuntivo 1',
''																								AS 'Stato di cittadinanza aggiuntivo 2',
''																								AS 'Zona/Frazione di residenza',
''																								AS 'Zona/Frazione di domicilio',
''																								AS 'Nome utente'          
FROM StoricoAnagrafica
inner join Anagrafica				  ON Anagrafica.id = StoricoAnagrafica.IDAnagrafica
LEFT JOIN TabCom AS TabCom_Nascita    ON TabCom_Nascita.Descrizione = Anagrafica.LuogoNascita AND TabCom_Nascita.Prov = Anagrafica.ProvNascita
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = Anagrafica.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = Anagrafica.IDComuneDom
LEFT JOIN AnagAltreInfo               ON AnagAltreInfo.IDAnagrafica = StoricoAnagrafica.IDAnagrafica
LEFT JOIN StatiCivili                 ON StatiCivili.ID = StoricoAnagrafica.IDStatoCivile AND StatiCivili.Descrizione IS NOT NULL AND StatiCivili.Descrizione <> ''
--LEFT JOIN CatProtette                 ON CatProtette.Descrizione = AnagAltreInfo.Invalidita
LEFT JOIN INFINITY_ANAGCATPROTETTE    ON AnagAltreInfo.Invalidita = INFINITY_ANAGCATPROTETTE.Descrizione
--LEFT JOIN Nazioni                     ON Nazioni.DescNazione = AnagAltreInfo.Nazionalita
LEFT JOIN INFINITY_NAZIONALITA Nazioni  ON DescNazionalita = AnagAltreInfo.Nazionalita
WHERE
	-- Anagrafica.IDStato<>64 AND --ESCLUDO I FORMATORI ESTERNI
	-------------Anagrafica.id not in (SELECT distinct IDAnagrafica FROM Form_Formatori) AND    -- ESCLUDO I FORMATORI

	Anagrafica.id not in (select a.ID from Form_Formatori FF INNER JOIN Anagrafica A ON A.ID=FF.IDAnagrafica WHERE A.IDStato=64) AND    -- ESCLUDO I FORMATORI interni

	StoricoAnagrafica.IDAnagrafica IS NOT NULL  AND
--	StoricoAnagrafica.CodiceFiscale IS not NULL AND
	StoricoAnagrafica.Cognome IS NOT NULL       AND
	StoricoAnagrafica.Cognome <> ''             AND
	StoricoAnagrafica.Cognome <> '-'            AND
	StoricoAnagrafica.Nome IS NOT NULL          AND
	StoricoAnagrafica.Nome <> ''                AND
	StoricoAnagrafica.Nome <> '-'      
	and anagrafica.ID in (90,276,300,307,312)
ORDER BY StoricoAnagrafica.IDAnagrafica, StoricoAnagrafica.DallaData


SELECT 
'B0101'																				 AS 'Tipo record',
'A'																					 AS 'Modalit� operativa',
CASE
    WHEN MAX(A.DATANASCITA) IS NULL THEN '01/01/1800'
    ELSE FORMAT(MAX(A.DATANASCITA),'dd/MM/yyyy')
END																					 AS 'Data Validit�',
'000000'																			 AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), FF.IDAnagrafica),16)				 AS 'Codice soggetto',
MAX(a.Cognome)																		 AS 'Cognome/ragione sociale',
MAX(a.Nome)																			 AS 'Nome/ragione sociale2',
CASE
    WHEN upper(isnull(MAX(a.Sesso),'')) = 'M' THEN 'M'
    WHEN upper(isnull(MAX(a.Sesso),'')) = 'F' THEN 'F'
    ELSE ''
END																					 AS 'Sesso',
CASE
    WHEN MAX(A.DATANASCITA) IS NULL THEN ''
    ELSE FORMAT(MAX(A.DATANASCITA),'dd/MM/yyyy')
END																					 AS 'Data di nascita/data costituzione',
isnull(MAX(a.LuogoNascita),'')															 AS 'Localit�',
''																					 AS 'CAP',
isnull(MAX(a.ProvNascita),'')															 AS 'Provincia',
CONVERT(varchar(10),UPPER(ISNULL(substring(MAX(a.CodiceFiscale),12,4) ,'')))				 AS 'Codice catastale',
CASE
	WHEN UPPER(ISNULL(substring(MAX(a.CodiceFiscale),12,1) ,'')) = 'Z' THEN
		''
	ELSE
		'IT'
END																					 AS 'Stato di nascita',
isnull(MAX(a.CodiceFiscale),'')															 AS 'CodiceFiscale',
isnull(MAX(a.PartitaIVA),'')																 AS 'PartitaIVA',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(MAX(a.Indirizzo),'')))))					 AS 'Indirizzo', /* residenza*/
CONVERT(varchar(6),UPPER(ISNULL(MAX(a.NumCivico),'')))									 AS 'N� civico',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Residenza.Descrizione),MAX(a.Comune))))			 AS 'Comune',
CONVERT(varchar(10),ISNULL(MAX(TabCom_Residenza.Cap),MAX(a.Cap)))								 AS 'Cap',
CONVERT(varchar(10),UPPER(ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia))))				 AS 'Provinci',
CONVERT(varchar(4),UPPER(ISNULL(MAX(TabCom_Residenza.Codice),'')))						 AS 'Codice catastale',
CASE
	WHEN ISNULL(MAX(TabCom_Residenza.Codice),'') <> '' or (ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia)) <> ''  or NOT ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia)) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = MAX(a.Stato)),''))
	ELSE
		''
END																					 AS 'Stato',
''																					 AS 'Prefisso',
CONVERT(varchar(20),ISNULL(MAX(a.RecapitiTelefonici),''))								 AS 'Telefono',
CONVERT(varchar(20),ISNULL(MAX(a.Fax),''))												 AS 'Fax',
CONVERT(varchar(20),ISNULL(MAX(a.Cellulare),''))											 AS 'cellulare',
CONVERT(varchar(70),LOWER(LTRIM(RTRIM(ISNULL(MAX(a.Email),'')))))						 AS 'E-mail',
''																					 AS 'Indirizzo Web',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(MAX(a.DomicilioIndirizzo),'')))))			 AS 'Indirizzo',  /*domicilio*/
CONVERT(varchar(6),UPPER(ISNULL(MAX(a.DomicilioNumCivico),'')))							 AS 'N� civico domicilio',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Domicilio.Descrizione),MAX(a.DomicilioComune))))	 AS 'Comune domicilio',
CONVERT(varchar(10),ISNULL(MAX(TabCom_Domicilio.Cap),''))								 AS 'Cap domicilio',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia))))	 AS 'Provincia domicilio',
CONVERT(varchar(4),UPPER(ISNULL(MAX(TabCom_Domicilio.Codice),'')))                        AS 'Codice catastale domicilio',
CASE
	WHEN ISNULL(MAX(TabCom_Domicilio.Codice),'') <> '' or (ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia)) <> ''  or NOT ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia)) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = MAX(a.DomicilioStato)),''))
	ELSE
		''
END																					 AS 'Stato domicilio',
''																					 AS 'Prefisso domicilio',
''																					 AS 'Telefono domicilio',
''																					 AS 'Fax domicilio',
CASE
	when MAX(A.IDStato)=64 THEN
		'FORE'
	ELSE
		'FORI'
END																					 AS 'Tipo soggetto',
''																					 AS 'Natura del soggetto',
RIGHT('000000' + CONVERT(varchar(6), FF.IDAnagrafica),6)							 AS 'Codice esterno',
''																					 AS 'Tipologia1',
''																					 AS 'Tipologia2',
''																					 AS 'Tipologia3'
from Form_Formatori FF
INNER JOIN Anagrafica A ON A.ID=FF.IDAnagrafica
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = a.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = a.IDComuneDom
--WHERE a.ID=74
WHERE A.IDStato=64
GROUP BY IDAnagrafica

union

select
'B0101'																				 AS 'Tipo record',
'A'																					 AS 'Modalit� operativa',
CASE
    WHEN A.DataNascita IS NULL THEN '01/01/1800'
    ELSE FORMAT(A.DataNascita,'dd/MM/yyyy')
END																					 AS 'Data Validit�',
'000000'																			 AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), a.ID),16)				 AS 'Codice soggetto',
a.Cognome																 AS 'Cognome/ragione sociale',
a.Nome																			 AS 'Nome/ragione sociale2',
CASE
    WHEN upper(isnull(a.sesso,'')) = 'M' THEN 'M'
    WHEN upper(isnull(a.sesso,'')) = 'F' THEN 'F'
    ELSE ''
END																					 AS 'Sesso',
CASE
    WHEN A.DATANASCITA IS NULL THEN ''
    ELSE FORMAT(A.DATANASCITA,'dd/MM/yyyy')
END																					 AS 'Data di nascita/data costituzione',
isnull(a.LuogoNascita,'')															 AS 'Localit�',
''																					 AS 'CAP',
isnull(a.ProvNascita,'')															 AS 'Provincia',
CONVERT(varchar(10),UPPER(ISNULL(substring(a.CodiceFiscale,12,4) ,'')))				 AS 'Codice catastale',
CASE
	WHEN UPPER(ISNULL(substring(a.CodiceFiscale,12,1) ,'')) = 'Z' THEN
		''
	ELSE
		'IT'
END																					 AS 'Stato di nascita',
isnull(a.CodiceFiscale,'')															 AS 'CodiceFiscale',
isnull(a.PartitaIVA,'')																 AS 'PartitaIVA',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(a.Indirizzo,'')))))					 AS 'Indirizzo', /* residenza*/
CONVERT(varchar(6),UPPER(ISNULL(a.NumCivico,'')))									 AS 'N� civico',
CONVERT(varchar(40),UPPER(ISNULL(TabCom_Residenza.Descrizione,a.Comune)))			 AS 'Comune',
CONVERT(varchar(10),ISNULL(TabCom_Residenza.Cap,a.Cap))								 AS 'Cap',
CONVERT(varchar(10),UPPER(ISNULL(TabCom_Residenza.Prov,a.Provincia)))				 AS 'Provinci',
CONVERT(varchar(4),UPPER(ISNULL(TabCom_Residenza.Codice,'')))						 AS 'Codice catastale',
CASE
	WHEN ISNULL(TabCom_Residenza.Codice,'') <> '' or (ISNULL(TabCom_Residenza.Prov,a.Provincia) <> ''  or NOT ISNULL(TabCom_Residenza.Prov,a.provincia) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = a.Stato),''))
	ELSE
		''
END																					 AS 'Stato',
''																					 AS 'Prefisso',
CONVERT(varchar(20),ISNULL(a.RecapitiTelefonici,''))								 AS 'Telefono',
CONVERT(varchar(20),ISNULL(a.Fax,''))												 AS 'Fax',
CONVERT(varchar(20),ISNULL(a.Cellulare,''))											 AS 'cellulare',
CONVERT(varchar(70),LOWER(LTRIM(RTRIM(ISNULL(a.Email,'')))))						 AS 'E-mail',
''																					 AS 'Indirizzo Web',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(a.DomicilioIndirizzo,'')))))			 AS 'Indirizzo',  /*domicilio*/
CONVERT(varchar(6),UPPER(ISNULL(a.DomicilioNumCivico,'')))							 AS 'N� civico domicilio',
CONVERT(varchar(40),UPPER(ISNULL(TabCom_Domicilio.Descrizione,a.DomicilioComune)))	 AS 'Comune domicilio',
CONVERT(varchar(10),ISNULL(TabCom_Domicilio.Cap,''))								 AS 'Cap domicilio',
CONVERT(varchar(40),UPPER(ISNULL(TabCom_Domicilio.Prov,a.DomicilioProvincia)))	 AS 'Provincia domicilio',
CONVERT(varchar(4),UPPER(ISNULL(TabCom_Domicilio.Codice,'')))                        AS 'Codice catastale domicilio',
CASE
	WHEN ISNULL(TabCom_Domicilio.Codice,'') <> '' or (ISNULL(TabCom_Domicilio.Prov,a.DomicilioProvincia) <> ''  or NOT ISNULL(TabCom_Domicilio.Prov,a.DomicilioProvincia) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = a.DomicilioStato),''))
	ELSE
		''
END																					 AS 'Stato domicilio',
''																					 AS 'Prefisso domicilio',
''																					 AS 'Telefono domicilio',
''																					 AS 'Fax domicilio',
CASE
	when A.IDStato=64 THEN
		'FORE'
	ELSE
		'FORI'
END																					 AS 'Tipo soggetto',
''																					 AS 'Natura del soggetto',
RIGHT('000000' + CONVERT(varchar(6), a.ID),6)							 AS 'Codice esterno',
''																					 AS 'Tipologia1',
''																					 AS 'Tipologia2',
''																					 AS 'Tipologia3'

FROM Anagrafica a
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = a.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = a.IDComuneDom
 where idstato=64
and a.id not in (select idanagrafica from Form_Formatori)










/*****************************************************************************************************************/
/****************************************** FORMAZIONE- TIPI SOGGETTO ********************************************/
/*****************************************************************************************************************/


;SELECT 
'B0102'																				 AS 'Tipo record',
'C'																					 AS 'Modalit� operativa',
'000000'																			 AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), FF.IDAnagrafica),16)				 AS 'Codice soggetto',
'01/01/1800'																		 AS 'Data inizio Validit� dei dati',
'31/12/2999'  																		 AS 'Data fine Validit� dei dati',
CASE
	when MAX(A.IDStato)=64 THEN
		'FORE'
	ELSE
		'FORI'
END																					 AS 'Tipo soggetto',
''																					 AS 'Codice esterno',
''																					 AS 'Tipologia1',
''																					 AS 'Tipologia2',
''																					 AS 'Tipologia3'
from Form_Formatori FF
INNER JOIN Anagrafica A ON A.ID=FF.IDAnagrafica
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = a.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = a.IDComuneDom
WHERE a.IDStato<>64
GROUP BY IDAnagrafica



