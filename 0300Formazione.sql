/* PRINCIPALE = FORMAZIONE */

/*****************************************************************************************************************/
/*************************************** FORMAZIONE-AREE DI FORMAZIONE*  *****************************************/
/*****************************************************************************************************************/

;SELECT
'A02'																				 AS 'Tipo record',
'C'																					 AS 'Modalità operativa',
'AREE_FORMAZ'																		 AS 'Tabella di riferimento',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)									 AS 'Codice esterno area tematica',
Descrizione																			 AS 'Descrizione centro costo',
''																					 AS 'Codice esterno area tematica di riferimento',
'000000'				  															 AS 'Codice azienda',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)									 AS 'Codice area'
FROM Aree
UNION
SELECT
'A02'																				 AS 'Tipo record',
'C'																					 AS 'Modalità operativa',
'AREE_FORMAZ'																		 AS 'Tabella di riferimento',
'9999999999'																		 AS 'Codice esterno area tematica',
'AREA GENERICA IMPORTAZIONE'														 AS 'Descrizione centro costo',
''																					 AS 'Codice esterno area tematica di riferimento',
'000000'				  															 AS 'Codice azienda',
'9999999999'																		 AS 'Codice area'

/*****************************************************************************************************************/
/*************************************** FORMAZIONE-TIPO DI FORMAZIONE*  *****************************************/
/*****************************************************************************************************************/

;select 
'A02'																				 AS 'Tipo record',
'C'																					 AS 'Modalità operativa',
'TIPO_FORMAZ'																		 AS 'Tabella di riferimento',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)									 AS 'Codice attività',
Descrizione																			 AS 'Descrizione',
'000000'				  															 AS 'Codice azienda',
'N'																					 AS 'Specifica date',
'N'																					 AS 'Approvazione del responsabile necessaria'
from Form_TipoCorso

/*****************************************************************************************************************/
/****************************************** FORMAZIONE-AULE DEL CORSO*  ******************************************/
/*****************************************************************************************************************/

;SELECT 
'A02'																				 AS 'Tipo record',
'C'																					 AS 'Modalità operativa',
'HALL_FORMAZ'																		 AS 'Tabella di riferimento',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)									 AS 'Codice attività',
Descrizione																			 AS 'Descrizione',
''																					 AS 'Descrizione breve',
''																					 AS 'Numero posti',
isnull(referente,'')																 AS 'Interlocutore',
isnull(indirizzo,'')																 AS 'Indirizzo',
''																					 AS 'N° civico',
'IT'																				 AS 'Cod.stato',
''																					 AS 'Cod.catastale comune',
''																					 AS 'Comune',
'000000'				  															 AS 'Codice azienda',
''																					 AS 'Videoconferenza'
FROM INFINITY_AULECORSI


--SELECT * FROM INFINITY_AULECORSI

/*****************************************************************************************************************/
/**************************************** FORMAZIONE-TIPOLOGIA FORMATORI*  ***************************************/
/*****************************************************************************************************************/

;SELECT 
'A02'																				 AS 'Tipo record',
'C'																					 AS 'Modalità operativa',
'TIPO_FORMAT'																		 AS 'Tabella di riferimento',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)									 AS 'Codice',
Descrizione																			 AS 'Descrizione',
CASE
    WHEN DESCRIZIONE = 'formatore esterno' THEN 'D'
    WHEN DESCRIZIONE = 'in Congedo parentale' THEN 'D'
    ELSE 'I'
END																					 AS 'Tipologia standard'
FROM INFINITY_TIPOLOGIAFORMATORI

/*****************************************************************************************************************/
/************************************* FORMAZIONE-DOCENTI INTERNI/ESTERNI*  **************************************/
/*****************************************************************************************************************/

/*
;SELECT 
'B0101'																				 AS 'Tipo record',
'A'																					 AS 'Modalità operativa',
CASE
    WHEN MAX(A.DATANASCITA) IS NULL THEN '01/01/1800'
    ELSE FORMAT(MAX(A.DATANASCITA),'dd/MM/yyyy')
END																					 AS 'Data Validità',
'000000'																			 AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), FF.IDAnagrafica),16)				 AS 'Codice soggetto',
MAX(a.Cognome)																		 AS 'Cognome/ragione sociale',
MAX(a.Nome)																			 AS 'Nome/ragione sociale2',
CASE
    WHEN upper(isnull(MAX(a.Sesso),'')) = 'M' THEN 'M'
    WHEN upper(isnull(MAX(a.Sesso),'')) = 'F' THEN 'F'
    ELSE ''
END																					 AS 'Sesso',
CASE
    WHEN MAX(A.DATANASCITA) IS NULL THEN ''
    ELSE FORMAT(MAX(A.DATANASCITA),'dd/MM/yyyy')
END																					 AS 'Data di nascita/data costituzione',
isnull(MAX(a.LuogoNascita),'')															 AS 'Località',
''																					 AS 'CAP',
isnull(MAX(a.ProvNascita),'')															 AS 'Provincia',
CONVERT(varchar(10),UPPER(ISNULL(substring(MAX(a.CodiceFiscale),12,4) ,'')))				 AS 'Codice catastale',
CASE
	WHEN UPPER(ISNULL(substring(MAX(a.CodiceFiscale),12,1) ,'')) = 'Z' THEN
		''
	ELSE
		'IT'
END																					 AS 'Stato di nascita',
isnull(MAX(a.CodiceFiscale),'')															 AS 'CodiceFiscale',
isnull(MAX(a.PartitaIVA),'')																 AS 'PartitaIVA',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(MAX(a.Indirizzo),'')))))					 AS 'Indirizzo', /* residenza*/
CONVERT(varchar(6),UPPER(ISNULL(MAX(a.NumCivico),'')))									 AS 'N° civico',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Residenza.Descrizione),MAX(a.Comune))))			 AS 'Comune',
CONVERT(varchar(10),ISNULL(MAX(TabCom_Residenza.Cap),MAX(a.Cap)))								 AS 'Cap',
CONVERT(varchar(10),UPPER(ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia))))				 AS 'Provinci',
CONVERT(varchar(4),UPPER(ISNULL(MAX(TabCom_Residenza.Codice),'')))						 AS 'Codice catastale',
CASE
	WHEN ISNULL(MAX(TabCom_Residenza.Codice),'') <> '' or (ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia)) <> ''  or NOT ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia)) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = MAX(a.Stato)),''))
	ELSE
		''
END																					 AS 'Stato',
''																					 AS 'Prefisso',
CONVERT(varchar(20),ISNULL(MAX(a.RecapitiTelefonici),''))								 AS 'Telefono',
CONVERT(varchar(20),ISNULL(MAX(a.Fax),''))												 AS 'Fax',
CONVERT(varchar(20),ISNULL(MAX(a.Cellulare),''))											 AS 'cellulare',
CONVERT(varchar(70),LOWER(LTRIM(RTRIM(ISNULL(MAX(a.Email),'')))))						 AS 'E-mail',
''																					 AS 'Indirizzo Web',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(MAX(a.DomicilioIndirizzo),'')))))			 AS 'Indirizzo',  /*domicilio*/
CONVERT(varchar(6),UPPER(ISNULL(MAX(a.DomicilioNumCivico),'')))							 AS 'N° civico domicilio',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Domicilio.Descrizione),MAX(a.DomicilioComune))))	 AS 'Comune domicilio',
CONVERT(varchar(10),ISNULL(MAX(TabCom_Domicilio.Cap),''))								 AS 'Cap domicilio',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia))))	 AS 'Provincia domicilio',
CONVERT(varchar(4),UPPER(ISNULL(MAX(TabCom_Domicilio.Codice),'')))                        AS 'Codice catastale domicilio',
CASE
	WHEN ISNULL(MAX(TabCom_Domicilio.Codice),'') <> '' or (ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia)) <> ''  or NOT ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia)) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = MAX(a.DomicilioStato)),''))
	ELSE
		''
END																					 AS 'Stato domicilio',
''																					 AS 'Prefisso domicilio',
''																					 AS 'Telefono domicilio',
''																					 AS 'Fax domicilio',
'FORE'																				 AS 'Tipo soggetto',
''																					 AS 'Natura del soggetto',
''																					 AS 'Codice esterno',
''																					 AS 'Tipologia1',
''																					 AS 'Tipologia2',
''																					 AS 'Tipologia3'
from Form_Formatori FF
INNER JOIN Anagrafica A ON A.ID=FF.IDAnagrafica
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = a.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = a.IDComuneDom
WHERE a.IDStato=64
GROUP BY IDAnagrafica

*/

/*****************************************************************************************************************/
/****************************************** FORMAZIONE- TIPI SOGGETTO ********************************************/
/*****************************************************************************************************************/

/*

;SELECT 
'B0102'																				 AS 'Tipo record',
'C'																					 AS 'Modalità operativa',
'000000'																			 AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), FF.IDAnagrafica),16)				 AS 'Codice soggetto',
'01/01/1800'																		 AS 'Data inizio Validità dei dati',
'31/12/2999'  																		 AS 'Data fine Validità dei dati',
'FORI'																				 AS 'Tipo soggetto',
''																					 AS 'Codice esterno',
''																					 AS 'Tipologia1',
''																					 AS 'Tipologia2',
''																					 AS 'Tipologia3'
from Form_Formatori FF
INNER JOIN Anagrafica A ON A.ID=FF.IDAnagrafica
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = a.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = a.IDComuneDom
WHERE a.IDStato<>64
GROUP BY IDAnagrafica

/*****************************************************************************************************************/
/*************************************** FORMAZIONE-CATALOGO FORMAZIONE ******************************************/
/*****************************************************************************************************************/

*/

;SELECT 
'C01'																								 AS 'Tipo record',
'C'																									 AS 'Modalità operativa',
RIGHT('000000' + CONVERT(varchar(6), fa.ID),6)														 AS 'Codice attività formativa'
,fa.Descrizione																			   		     AS 'Descrizione',
''																									 AS 'Descrizione visibile sul calendario',
RIGHT('0000000000' + CONVERT(varchar(10), ft.ID),10)							 					 AS 'Codice tipo di formazione',
CASE   
    WHEN fa.IDArea = 0 THEN '9999999999'   
	WHEN fa.IDArea is null THEN '9999999999'
    else
	isnull(RIGHT('0000000000' + CONVERT(varchar(10), fa.IDArea),10),'')
END																									AS 'Area di formazione',
/*FORMAT(fa.DataInizio,'dd/MM/yyyy')																	 AS 'Data inizio validità',
FORMAT(fa.DataFine,'dd/MM/yyyy')																	 AS 'Data fine validità',*/
'01/01/1800'																						 AS 'Data inizio validità',
'31/12/2999'																						 AS 'Data fine validità',
REPLACE(CAST(Programma AS NVARCHAR(MAX)), CHAR(13) + CHAR(10), '\n ')								  AS 'Programma e contenuti',
''																									 AS 'Obiettivi',
''																									 AS 'Strutturazione attività',
''																									 AS 'Ente di finanziamento',       /* ???? */
''																									 AS 'Piano di finanziamento',
''																			        				 AS 'Codice azienda',
''																									 AS 'Obbligo visualizzazione dei documenti',
''																									 AS 'Videoconferenza',
''																									 AS 'Corso disponibile per richieste formative'

FROM Form_Attivita fa
 inner join Form_TipoCorso ft on ft.Descrizione = fa.Tipo
 --where fa.id=36


/*****************************************************************************************************************/
/********************************** FORMAZIONE-CATALOGO FORMAZIONE - RUOLI ***************************************/
/*****************************************************************************************************************/

/* ?????????????????? */


/*****************************************************************************************************************/
/********************************* FORMAZIONE-CATALOGO FORMAZIONE-COMPETENZE*  ***********************************/
/*****************************************************************************************************************/

/* ?????????? */
/*
;SELECT
'C06'																				 AS 'Tipo record',
'A'																					 AS 'Modalità operativa',
RIGHT('000000' + CONVERT(varchar(6), Form_AttivitaComp.IDCorso),6)					 AS 'Codice attività formativa',
RIGHT('0000000000' + CONVERT(varchar(10), Form_AttivitaComp.ID),10)					 AS 'Codice competenza',
''																					 AS 'Codice azienda attività a catalogo',
''																					 AS 'Codice azienda attività a catalogo'
from Form_AttivitaComp,Competenze
INNER JOIN AreeCompetenze
ON Competenze.IDArea=AreeCompetenze.ID
WHERE Form_AttivitaComp.IDCompetenza=Competenze.ID
*/
--and Form_AttivitaComp.IDCorso=36


/*****************************************************************************************************************/
/************************************** FORMAZIONE-PIANI DI FORMAZIONE*  ****************************************/
/*****************************************************************************************************************/

;SELECT
'C15'																 								AS 'Tipo record',
'A'																	 								AS 'Modalità operativa',
'0000000000' + CONVERT(varchar(10), ID)							 									AS 'Codice',
'PF_' + Descrizione												 									AS 'Descrizione piano',
'000000'																							AS 'Codice azienda',
''																									AS 'Data creazione',
'01/01/' + Descrizione																				AS 'Data inizio validità',
'31/12/' + Descrizione																				AS 'Data fine validità',
'I'																									AS 'Stato del piano',
''																									AS 'Data fine validità',
'000000'																							AS 'Codice azienda RDL',
''																									AS 'Pianificato',
'N'																									AS 'Visibilita status al collaboratore'
from INFINITY_PIANOFORMAZIONE

/*****************************************************************************************************************/
/************************************** FORMAZIONE-EDIZIONI DA CATALOGO*  ****************************************/
/*****************************************************************************************************************/


/*****************************************************************************************************************/
/************************************** FORMAZIONE-EDIZIONI DA CATALOGO*  ****************************************/
/*****************************************************************************************************************/

;SELECT
'C20'																 															AS 'Tipo record',
'A'																	 															AS 'Modalità operativa',
RIGHT('0000000000' + CONVERT(varchar(10), fae.ID),10)				 															AS 'Codice edizione',
RIGHT('000000' + CONVERT(varchar(6), fae.IDCorso),6)																			AS 'Codice attività formativa',
'S'																																AS 'Da catalogo',
ISNULL(FORMAT(GETDATE(),'dd/MM/yyyy'),'')						  																AS 'Data inserimento',
'N'																																AS 'Annullato',      /* ????? */
'N'								    																							AS 'Da pubblicare',   /* ????? */
isnull(RIGHT('0000000000' + CONVERT(varchar(10), pf.ID),10),RIGHT('0000000000' + CONVERT(varchar(10), pf2.ID),10))				AS 'Codice piano di formazione',
''																																AS 'Edizione',
isnull(FORMAT(
(select MIN(Data)
from Form_AttivitaDate
where Form_AttivitaDate.IDEdizione = fae.id)
,'dd/MM/yyyy'),'')																												AS 'Data inizio attività',		
isnull(FORMAT(
(select MAX(Data)
from Form_AttivitaDate
where Form_AttivitaDate.IDEdizione = fae.id)
,'dd/MM/yyyy'),'')																												AS 'Data fine attività',		

replace((select sum(OreLav) + sum(OreExtra) + sum(OresTRAORD) from Form_AttivitaDate where IDEdizione = fae.ID),',','.')        AS 'Ore totali pagate',


'0'																																AS 'Ore totali non pagate',
replace((select sum(OreLav) + sum(OreExtra) + sum(OresTRAORD) from Form_AttivitaDate where IDEdizione = fae.ID),',','.')  		AS 'Ore lavorate',
''																																AS 'Tipo docente standard',  /* ???? */
''																																AS 'Docente',  /* ???? */
'A'																													  		    AS 'Tipo sede',
ISNULL(fae.LuogoSvolgimento,'')																						  		    AS 'Sede',
''																										  						AS 'N° massimo iscritti',
''	    																						 					  		    AS 'Stato',
''																																AS 'Area geografica',
''																								 					  		    AS 'Codice azienda',
''																								 					  		    AS 'Codice azienda piano di formazione',
'N'																								 					  		    AS 'Crediti formativi',  /* ???? */
'N'																								 					  		    AS 'Considera nel budget',
''																													  		    AS 'Codice richiesta',
''																													  		    AS 'Codice azienda catalogo'
FROM Form_AttivitaEdizioni fae
inner join Form_Attivita fa on fa.id = fae.IDCorso
left join INFINITY_PIANOFORMAZIONE pf on pf.Descrizione =  DATEPART(YEAR,Previsionale_DataInizio)
left join INFINITY_PIANOFORMAZIONE pf2 on pf2.Descrizione =  YEAR(GETDATE())
--WHERE fa.IDArea IS NOT NULL
--where faE.idCORSO in (36)

--select * from Form_AttivitaEdizioni


/*****************************************************************************************************************/
/**************************************** FORMAZIONE-EDIZIONI-DATE CERTE *****************************************/
/*****************************************************************************************************************/

;select 
'C23'																 AS 'Tipo record',
'A'																	 AS 'Modalità operativa',
RIGHT('0000000000' + CONVERT(varchar(10), IDEdizione),10)			 AS 'Codice edizione',
isnull(FORMAT(Data,'dd/MM/yyyy'),'')								 AS 'Data',
replace(isnull(fad.OreLav,'0'),',','.')								 AS 'Ore pagate',
''																	 AS 'Ore non pagate',
replace(isnull(fad.OreLav,'0'),',','.')								 AS 'Ore lavorative',

CASE DATEPART(HOUR, OraInizio)
    WHEN 0 THEN  '00'
    WHEN 1 THEN   '01'
    WHEN 2 THEN   '02'
    WHEN 3 THEN   '03'
    WHEN 4 THEN   '04'
    WHEN 5 THEN   '05'
    WHEN 6 THEN   '06'
    WHEN 7 THEN   '07'
    WHEN 8 THEN   '08'
    WHEN 9 THEN   '09'
	ELSE CONVERT(varchar, DATEPART(HOUR, OraInizio))
END + ':' + 
CASE DATEPART(minute, OraInizio)
    WHEN 0 THEN  '00'
    WHEN 1 THEN   '01'
    WHEN 2 THEN   '02'
    WHEN 3 THEN   '03'
    WHEN 4 THEN   '04'
    WHEN 5 THEN   '05'
    WHEN 6 THEN   '06'
    WHEN 7 THEN   '07'
    WHEN 8 THEN   '08'
    WHEN 9 THEN   '09'
	ELSE CONVERT(varchar, DATEPART(minute, OraInizio))
END AS 'Ora inizio',

CASE DATEPART(HOUR, OraFine)
    WHEN 0 THEN  '00'
    WHEN 1 THEN   '01'
    WHEN 2 THEN   '02'
    WHEN 3 THEN   '03'
    WHEN 4 THEN   '04'
    WHEN 5 THEN   '05'
    WHEN 6 THEN   '06'
    WHEN 7 THEN   '07'
    WHEN 8 THEN   '08'
    WHEN 9 THEN   '09'
	ELSE CONVERT(varchar, DATEPART(HOUR, OraFine))
END + ':' + 
CASE DATEPART(minute, OraFine)
    WHEN 0 THEN  '00'
    WHEN 1 THEN   '01'
    WHEN 2 THEN   '02'
    WHEN 3 THEN   '03'
    WHEN 4 THEN   '04'
    WHEN 5 THEN   '05'
    WHEN 6 THEN   '06'
    WHEN 7 THEN   '07'
    WHEN 8 THEN   '08'
    WHEN 9 THEN   '09'
	ELSE CONVERT(varchar, DATEPART(minute, OraFine))
END AS 'Ora fine',
''																	 AS 'Codice azienda edizione'
from Form_AttivitaDate fad
inner join Form_AttivitaEdizioni fae on fad.idedizione = fae.id
inner join Form_Attivita fa on fa.id = fae.IDCorso
--and IDEdizione in (39,40)
and fad.idedizione NOT IN (
select Form_AttivitaEdizioni.ID
from Form_Formatori
join Anagrafica  on Form_Formatori.IDAnagrafica=Anagrafica.ID
join EBC_TipiStato on Anagrafica.IDTipoStato=EBC_TipiStato.ID
left join Form_EntiFormaz on Form_EntiFormaz.id=Form_Formatori.IDEnteFormaz
LEFT JOIN Form_Attivita ON Form_Attivita.ID = Form_Formatori.IDCorso
LEFT JOIN Form_AttivitaEdizioni ON Form_AttivitaEdizioni.IDCorso = Form_Attivita.ID
LEFT JOIN Form_AttivitaDate ON Form_AttivitaDate.IDEdizione = Form_AttivitaEdizioni.ID)


/*****************************************************************************************************************/
/********************************* FORMAZIONE-EDIZIONI-DATE CERTE CON DOCENTI ************************************/
/*****************************************************************************************************************/



;select 
'C24'																	 AS 'Tipo record',
'A'																		 AS 'Modalità operativa',
RIGHT('0000000000' + CONVERT(varchar(10), Form_AttivitaEdizioni.ID),10)	 AS 'Cod. edizione',
isnull(FORMAT(Form_AttivitaDate.Data,'dd/MM/yyyy'),'')					 AS 'Data',
replace(isnull(Form_AttivitaDate.OreLav,'0'),',','.')					 AS 'Ore pagate',
''																		 AS 'Ore non pagate',
replace(isnull(Form_AttivitaDate.OreLav,'0'),',','.')					 AS 'Ore lavorative',
isnull(FORMAT(OraInizio,'HH:mm'),'')									 AS 'Ora inizio',
isnull(FORMAT(OraFine,'HH:mm'),'')										 AS 'Ora fine',
'A'																		 AS 'Tipo sede',
''																		 AS 'Sede',

case
   when Anagrafica.IdStato = 64 then '0000000003'
   else '0000000001'
end																		 AS 'Tipologia1',
RIGHT('000000' + CONVERT(varchar(6), Form_Formatori.IDAnagrafica),6)	 AS 'Docente1',
''																		 AS 'Tipologia2',
''																		 AS 'Docente2',
''																		 AS 'Tipologia3',
''																		 AS 'Docente3',
''																		 AS 'Tipologia4',
''																		 AS 'Docente4',
''																		 AS 'Tipologia5',
''																		 AS 'Docente5',
''																		 AS 'Tipologia6',
''																		 AS 'Docente6',
''																		 AS 'Tipologia7',
''																		 AS 'Docente7',
''																		 AS 'Tipologia8',
''																		 AS 'Docente8',
''																		 AS 'Tipologia9',
''																		 AS 'Docente9',
''																		 AS 'Tipologia10',
''																		 AS 'Docente10',
''																		 AS 'Tipologia11',
''																		 AS 'Docente11',
''																		 AS 'Tipologia12',
''																		 AS 'Docente12',
''																		 AS 'Tipologia13',
''																		 AS 'Docente13',
''																		 AS 'Tipologia14',
''																		 AS 'Docente14',
''																		 AS 'Tipologia15',
''																		 AS 'Docente15',
''																		 AS 'Codice azienda edizione'
from Form_Formatori
join Anagrafica  on Form_Formatori.IDAnagrafica=Anagrafica.ID
join EBC_TipiStato on Anagrafica.IDTipoStato=EBC_TipiStato.ID
left join Form_EntiFormaz on Form_EntiFormaz.id=Form_Formatori.IDEnteFormaz
LEFT JOIN Form_Attivita ON Form_Attivita.ID = Form_Formatori.IDCorso
LEFT JOIN Form_AttivitaEdizioni ON Form_AttivitaEdizioni.IDCorso = Form_Attivita.ID
LEFT JOIN Form_AttivitaDate ON Form_AttivitaDate.IDEdizione = Form_AttivitaEdizioni.ID
--where Anagrafica.IdStato=64

/*****************************************************************************************************************/
/********************************* FORMAZIONE-EDIZIONI ISCRIZIONE PARTECIPANTI ***********************************/
/*****************************************************************************************************************/


;select 
'C30'																	 AS 'Tipo record',
'A'																		 AS 'Modalità operativa',
RIGHT('0000000000' + CONVERT(varchar(10), ae.idedizione),10)			 AS 'Edizione',
'000000'																 AS 'Codice azienda soggetto',
RIGHT('0000000000000000' + CONVERT(varchar(16), a.ID),16)				 AS 'Codice identificativo soggetto',
'N'																		 AS 'Rinuncia',
isnull(FORMAT(atda.Data,'dd/MM/yyyy'),'')								 AS 'Data',
replace(isnull(ad.OrePres,'0'),',','.')									 AS 'Ore pagate',
''																		 AS 'Ore non pagate',
replace(isnull(ad.OrePres,'0'),',','.')									 AS 'Ore lavorative',
isnull(ad.Annotazioni,'')												 AS 'Note',
''																		 AS 'Numero ordine',
''																		 AS 'Data ordine',
''																		 AS 'Punteggio',
''																		 AS 'Codice azienda edizione',
'I'																		 AS 'Status',
'N'																		 AS 'Iscritto in videoconferenza',
''																		 AS 'Professione',
''																		 AS 'Disciplina',
''																		 AS 'Esito',
''																		 AS 'Motivo rinuncia',
''																		 AS 'Data rinuncia'
from
Form_AnagEdizioni ae
join Anagrafica a on a.ID = ae.IDAnagrafica
left join Form_AnagDate ad on ad.IDAnagEdizioni=ae.id
left join Form_attivitaDate atda on atda.id=ad.IDAttivitaDate
left join Form_Sessione on Form_Sessione.id=atda.IDSessione
--where ae.idedizione in (39,40)
--and a.id=170

select * from Form_AnagDate

/*****************************************************************************************************************/
/********************************* FORMAZIONE-EDIZIONI ISCRIZIONE PARTECIPANTI ***********************************/
/*****************************************************************************************************************/

;select 
'C31'																	 AS 'Tipo record',
'A'																		 AS 'Modalità operativa',
RIGHT('0000000000' + CONVERT(varchar(10), ae.idedizione),10)			 AS 'Edizione',
'000000'																 AS 'Codice azienda soggetto',
RIGHT('0000000000000000' + CONVERT(varchar(16), a.ID),16)				 AS 'Codice identificativo soggetto',
isnull(FORMAT(atda.Data,'dd/MM/yyyy'),'')								 AS 'Data',
replace(isnull(ad.OrePres,'0'),',', '.')								 AS 'Ore pagate',
''																		 AS 'Ore non pagate',
replace(isnull(ad.OrePres,'0'),',', '.')								 AS 'Ore lavorative',
''																		 AS 'Motivo assenza',
''																		 AS 'Numero ordine',
''																		 AS 'Data ordine',
''																		 AS 'Codice azienda edizione'
from
Form_AnagEdizioni ae
join Anagrafica a on a.ID = ae.IDAnagrafica
left join Form_AnagDate ad on ad.IDAnagEdizioni=ae.id
left join Form_attivitaDate atda on atda.id=ad.IDAttivitaDate
left join Form_Sessione on Form_Sessione.id=atda.IDSessione
--where ae.idedizione in (39,40)
--and a.id=170

--select * from Form_AnagEdizioni where idanagrafica=12
--select * from Form_AttivitaEdizioni where id=42