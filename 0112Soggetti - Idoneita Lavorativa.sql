/* PRINCIPALE = SOGGETTI-MATRIALI IN DOTAZIONE */

/*****************************************************************************************************************/
/*************************************** SOGGETTI-MATRIALI IN DOTAZIONE  *****************************************/
/*****************************************************************************************************************/

SELECT
'B60'																  AS 'Tipo record',
'C'																	  AS 'Modalit� operativa',
'000000'		     												  AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), AV.IDAnagrafica),16)  AS 'Codice soggetto',
''																	  AS 'Tipo visita',
FORMAT(DataSvolgimento,'dd/MM/yyyy')								  AS 'Data visita',
Esito																  AS 'Giudizio di idoneit�',
''																	  AS 'Tipo medico',
''																	  AS 'Codice soggetto medico',
AV.Note																  AS 'Note'

from AnagVisite AV