
/*****************************************************************************************************************/
/************************************* FORMAZIONE-DOCENTI INTERNI/ESTERNI*  **************************************/
/*****************************************************************************************************************/


;SELECT 
'B0101'																				 AS 'Tipo record',
'A'																					 AS 'Modalità operativa',
CASE
    WHEN MAX(A.DATANASCITA) IS NULL THEN '01/01/1800'
    ELSE FORMAT(MAX(A.DATANASCITA),'dd/MM/yyyy')
END																					 AS 'Data Validità',
'000000'																			 AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), FF.IDAnagrafica),16)				 AS 'Codice soggetto',
MAX(a.Cognome)																		 AS 'Cognome/ragione sociale',
MAX(a.Nome)																			 AS 'Nome/ragione sociale2',
CASE
    WHEN upper(isnull(MAX(a.Sesso),'')) = 'M' THEN 'M'
    WHEN upper(isnull(MAX(a.Sesso),'')) = 'F' THEN 'F'
    ELSE ''
END																					 AS 'Sesso',
CASE
    WHEN MAX(A.DATANASCITA) IS NULL THEN ''
    ELSE FORMAT(MAX(A.DATANASCITA),'dd/MM/yyyy')
END																					 AS 'Data di nascita/data costituzione',
isnull(MAX(a.LuogoNascita),'')															 AS 'Località',
''																					 AS 'CAP',
isnull(MAX(a.ProvNascita),'')															 AS 'Provincia',
CONVERT(varchar(10),UPPER(ISNULL(substring(MAX(a.CodiceFiscale),12,4) ,'')))				 AS 'Codice catastale',
CASE
	WHEN UPPER(ISNULL(substring(MAX(a.CodiceFiscale),12,1) ,'')) = 'Z' THEN
		''
	ELSE
		'IT'
END																					 AS 'Stato di nascita',
isnull(MAX(a.CodiceFiscale),'')															 AS 'CodiceFiscale',
isnull(MAX(a.PartitaIVA),'')																 AS 'PartitaIVA',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(MAX(a.Indirizzo),'')))))					 AS 'Indirizzo', /* residenza*/
CONVERT(varchar(6),UPPER(ISNULL(MAX(a.NumCivico),'')))									 AS 'N° civico',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Residenza.Descrizione),MAX(a.Comune))))			 AS 'Comune',
CONVERT(varchar(10),ISNULL(MAX(TabCom_Residenza.Cap),MAX(a.Cap)))								 AS 'Cap',
CONVERT(varchar(10),UPPER(ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia))))				 AS 'Provinci',
CONVERT(varchar(4),UPPER(ISNULL(MAX(TabCom_Residenza.Codice),'')))						 AS 'Codice catastale',
CASE
	WHEN ISNULL(MAX(TabCom_Residenza.Codice),'') <> '' or (ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia)) <> ''  or NOT ISNULL(MAX(TabCom_Residenza.Prov),MAX(a.Provincia)) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = MAX(a.Stato)),''))
	ELSE
		''
END																					 AS 'Stato',
''																					 AS 'Prefisso',
CONVERT(varchar(20),ISNULL(MAX(a.RecapitiTelefonici),''))								 AS 'Telefono',
CONVERT(varchar(20),ISNULL(MAX(a.Fax),''))												 AS 'Fax',
CONVERT(varchar(20),ISNULL(MAX(a.Cellulare),''))											 AS 'cellulare',
CONVERT(varchar(70),LOWER(LTRIM(RTRIM(ISNULL(MAX(a.Email),'')))))						 AS 'E-mail',
''																					 AS 'Indirizzo Web',
CONVERT(varchar(40),UPPER(LTRIM(RTRIM(ISNULL(MAX(a.DomicilioIndirizzo),'')))))			 AS 'Indirizzo',  /*domicilio*/
CONVERT(varchar(6),UPPER(ISNULL(MAX(a.DomicilioNumCivico),'')))							 AS 'N° civico domicilio',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Domicilio.Descrizione),MAX(a.DomicilioComune))))	 AS 'Comune domicilio',
CONVERT(varchar(10),ISNULL(MAX(TabCom_Domicilio.Cap),''))								 AS 'Cap domicilio',
CONVERT(varchar(40),UPPER(ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia))))	 AS 'Provincia domicilio',
CONVERT(varchar(4),UPPER(ISNULL(MAX(TabCom_Domicilio.Codice),'')))                        AS 'Codice catastale domicilio',
CASE
	WHEN ISNULL(MAX(TabCom_Domicilio.Codice),'') <> '' or (ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia)) <> ''  or NOT ISNULL(MAX(TabCom_Domicilio.Prov),MAX(a.DomicilioProvincia)) = '') THEN
		CONVERT(varchar(5),ISNULL((select UPPER(Abbrev2) FROM Nazioni WHERE Nazioni.DescNazione = MAX(a.DomicilioStato)),''))
	ELSE
		''
END																					 AS 'Stato domicilio',
''																					 AS 'Prefisso domicilio',
''																					 AS 'Telefono domicilio',
''																					 AS 'Fax domicilio',
'FORE'																				 AS 'Tipo soggetto',
''																					 AS 'Natura del soggetto',
''																					 AS 'Codice esterno',
''																					 AS 'Tipologia1',
''																					 AS 'Tipologia2',
''																					 AS 'Tipologia3'
from Form_Formatori FF
INNER JOIN Anagrafica A ON A.ID=FF.IDAnagrafica
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = a.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = a.IDComuneDom
WHERE a.IDStato=64
GROUP BY IDAnagrafica



/*****************************************************************************************************************/
/****************************************** FORMAZIONE- TIPI SOGGETTO ********************************************/
/*****************************************************************************************************************/

;SELECT 
'B0102'																				 AS 'Tipo record',
'C'																					 AS 'Modalità operativa',
'000000'																			 AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), FF.IDAnagrafica),16)				 AS 'Codice soggetto',
'01/01/1800'																		 AS 'Data inizio Validità dei dati',
'31/12/2999'  																		 AS 'Data fine Validità dei dati',
'FORI'																				 AS 'Tipo soggetto',
''																					 AS 'Codice esterno',
''																					 AS 'Tipologia1',
''																					 AS 'Tipologia2',
''																					 AS 'Tipologia3'
from Form_Formatori FF
INNER JOIN Anagrafica A ON A.ID=FF.IDAnagrafica
LEFT JOIN TabCom AS TabCom_Residenza  ON TabCom_Residenza.ID = a.IDComuneRes
LEFT JOIN TabCom AS TabCom_Domicilio  ON TabCom_Domicilio.ID = a.IDComuneDom
WHERE a.IDStato<>64
GROUP BY IDAnagrafica