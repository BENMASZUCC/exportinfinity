/* PRINCIPALE = AZIENDA*/
/*****************************************************************************************************************/
/********************************************** ANAGRAFICHE - AZIENDA ********************************************/
/*****************************************************************************************************************/
;SELECT
'A01'																  AS 'Tipo record',
'C'																	  AS 'Modalit� operativa',
RIGHT('000000' + CONVERT(varchar(6), ID),6)   						  AS 'Codice area',
isnull(Descrizione,'') 											      AS 'Ragione sociale',
isnull(CodiceFiscale,'')											  AS 'Codice fiscale',
isnull(PartitaIVA,'')												  AS 'Partita IVA',
isnull(Indirizzo,'')												  AS 'Indirizzo sede legale',
isnull(NumCivico,'')												  AS 'N� civico',
isnull(Comune,'')													  AS 'Comune',
''																	  AS 'Codice catastale',
'IT'																  AS 'Codice stato',
isnull(CAP,'')														  AS 'CAP',
isnull(PrefissoInt,'')												  AS 'Prefisso telefonico',
isnull(TelefonoFisso,'')											  AS 'Telefono',
''																	  AS 'Fax',
isnull(Email,'')													  AS 'E-mail',
''																	  AS 'Indirizzo web',
'01/01/1800'														  AS 'data inizio validit�'
FROM Aziende



