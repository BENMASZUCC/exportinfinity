/* PRINCIPALE = RUOLI-MANSIONI */

/*****************************************************************************************************************/
/********************************************** RUOLI - DEFINIZIONE RUOLI ****************************************/
/*****************************************************************************************************************/

;SELECT
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalit� operativa',
'RUOLI'		     													  AS 'Tabella di riferimento',
RIGHT('0000000000' + CONVERT(varchar(10), ID),10)   				  AS 'Codice',
Descrizione			   											      AS 'Descrizione',
'01/01/1800'														  AS 'Data inizio validit�',
'31/12/2999'														  AS 'Data fine validit�',
'000000'															  AS 'Codice azienda/Ente'
from INFINITY_RUOLI
where descrizione is not null
--WHERE ID IN (704)
--and descrizione = 'Operatore Liv 1 2018 (Operatore Altri Servizi)'


IF OBJECT_ID('#TempRuoli') IS NOT NULL
drop table #TempRuoli

create table #TempRuoli
(
    ID varchar(10), 
    Descrizione Varchar(100)
)

INSERT INTO #TempRuoli select RIGHT('0000000000' + CONVERT(varchar(10), ID),10), DESCRIZIONE from INFINITY_RUOLI
--SELECT * FROM #TempRuoli



/*****************************************************************************************************************/
/***************************************** RUOLI - SOGGETTI ASSOCIATI AI RUOLI ***********************************/
/*****************************************************************************************************************/

DECLARE @DescrmansioneTOT VARCHAR(200)

declare @sdoID int
DECLARE @IDMansione int
DECLARE @NodoOrganigramma VARCHAR(200)
DECLARE @Descrmansione VARCHAR(100)
DECLARE @IDAnag int
DECLARE @IDperc int
DECLARE @MESE DATETIME
DECLARE @DALLADATA DATETIME
DECLARE @ALLADATA DATETIME
DECLARE @ALLADATA2 DATETIME
DECLARE @cancellato int


declare @sdoIDOLD int
DECLARE @IDMansioneOLD int
DECLARE @DescrmansioneOLD VARCHAR(100)
DECLARE @NodoOrganigrammaOLD VARCHAR(200)
DECLARE @IDAnagOLD int
DECLARE @MESEOLD DATETIME
DECLARE @IDpercOLD int
DECLARE @DALLADATAOLD DATETIME
DECLARE @ALLADATAOLD DATETIME
DECLARE @cancellatoOLD int

DECLARE @DATAFINERUOLO DATETIME

declare @i int

set @i=0

IF OBJECT_ID('#Temp') IS NOT NULL
drop table #Temp

create table #Temp
(
    ID int, 
    Mansione Varchar(100), 
    IDAnagrafica int,
    mese SmallDatetIme, 
    perc int,
    dalladata Datetime,
    alladata DateTime,
	cancellato int,
	prog int
)

IF OBJECT_ID('#Temp2') IS NOT NULL
drop table #Temp2

create table #Temp2
(
    ID int, 
    Mansione Varchar(100), 
    IDAnagrafica int,
    mese SmallDatetIme, 
    perc int,
    dalladata Datetime,
    alladata DateTime,
	cancellato int,
	prog int
)

SET @DescrmansioneOLD = ''    

DECLARE db_cursor CURSOR FOR 
	select
	sdo.ID,
	M.ID,
	m.Descrizione as 'Mansione',
	o.descrizione as 'Nodo',
	a.ID,
	sdo.mese,
	sdo.perc,
	dalladata=
	case 
	when sdo.cancellato=0 or sdo.cancellato is null then sdo.mese
	when sdo.cancellato=1 then NULL
	end,
	alladata=
	case 
	when sdo.cancellato=0 or sdo.cancellato is null then NULL
	when sdo.cancellato=1 then sdo.mese
	end
	,sdo.cancellato
	from
	storicodiporg sdo
	join anagrafica a on a.id=sdo.iddipendente 
	join organigramma o on o.id=sdo.idorganigramma
	left join mansioni m on m.id=o.IDMansione
	--where sdo.iddipendente=2
	 order by  sdo.iddipendente, o.descrizione, sdo.mese, perc

OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO @sdoIDOLD, @IDMansione, @Descrmansione, @NodoOrganigramma, @IDAnag, @mese, @IDperc, @DALLADATA, @ALLADATA, @cancellato

WHILE @@FETCH_STATUS = 0  
BEGIN  

      set @i=@i+1

	  INSERT INTO #Temp (ID, Mansione, IDAnagrafica,mese, perc,dalladata,alladata, prog, cancellato) VALUES (@IDMansione, @Descrmansione + ' (' +  @NodoOrganigramma + ')'	, @IDAnag, @mese, @IDperc, @DALLADATA, @ALLADATA, @i, @cancellato)
  	  INSERT INTO #Temp2 (ID, Mansione, IDAnagrafica,mese, perc,dalladata,alladata, prog, cancellato) VALUES (@IDMansione, @Descrmansione + ' (' +  @NodoOrganigramma + ')'	, @IDAnag, @mese, @IDperc, @DALLADATA, @ALLADATA, @i, @cancellato)

	  SET @IDMansioneOLD = @IDMansione
	  SET @DescrmansioneOLD = @Descrmansione
	  SET @NodoOrganigrammaOLD = @NodoOrganigramma
	  SET @IDAnagOLD = @IDAnag
	  SET @meseOLD = @mese
	  SET @IDpercOLD = @IDperc
	  SET @DALLADATAOLD = @DALLADATA
	  SET @ALLADATAOLD = @ALLADATA
	  SET @cancellatoOLD = @cancellato

      FETCH NEXT FROM db_cursor INTO @sdoIDOLD, @IDMansione, @Descrmansione, @NodoOrganigramma, @IDAnag, @mese, @IDperc, @DALLADATA, @ALLADATA, @cancellato

END 

CLOSE db_cursor  
DEALLOCATE db_cursor 

;SELECT 
'B03'																  AS 'Tipo record',
'C'																	  AS 'Modalit� operativa',
RIGHT('0000000000' + CONVERT(varchar(10), A.ID),10)			   		  AS 'Codice ruolo',
'000000'															  AS 'Azienda soggetto',
RIGHT('0000000000000000' + CONVERT(varchar(16), A.IDAnagrafica),16)	  AS 'Codice soggetto',
A.perc																  AS '% occupazione',
''																	  AS 'Tipo associazione', /*  (T=Titolare/S=Sosituto - Se vuoto verr� valorizzato con T) */
ISNULL(FORMAT(A.mese,'dd/MM/yyyy'),'01/01/1800')					  AS 'Inizio validit�',
ISNULL(FORMAT(A.datafine,'dd/MM/yyyy'),'31/12/2999')				  AS 'Fine validit�',
'000000'															  AS 'Codice azienda ruolo'
FROM(
select #TempRuoli.ID AS ID, T1.Mansione, T1.IDAnagrafica AS IDAnagrafica,T1.mese, T1.perc,T1.dalladata,T1.alladata, T1.prog, T1.cancellato cancellato, 
CASE 
 WHEN T2.cancellato=0 AND T1.Mansione=T2.Mansione THEN
	DATEADD(DAY, -1, T2.MESE)
 ELSE
    CASE
	 WHEN T2.cancellato=1 AND T1.Mansione=T2.Mansione THEN
  	  T2.MESE
	 ELSE NULL
	END
END AS datafine
 from #Temp T1
 LEFT join #Temp2 T2 on T2.prog = (T1.prog+1)
 join #TempRuoli on #TempRuoli.Descrizione = T1.Mansione
  ) A WHERE A.cancellato=0 and A.ID is not null