/*****************************************************************************************************************/
/********************************************** SOGGETTI ALTRI INDIRIZZI - AZIENDA *******************************/
/*****************************************************************************************************************/

--IF OBJECT_ID('#TempContattiAz') IS NOT NULL
--drop table #TempContattiAz


IF EXISTS(SELECT 1 FROM sys.tables 
          WHERE Name = 'TempContattiAz')
BEGIN
    DROP TAble TempContattiAz
END

create table TempContattiAz
(
	ID int,
	IDAnagrafica int,
	IDAzienda int,
	IDCampoContattoAz int,
	voce1 varchar(400) NULL,
	voce2 varchar(400) NULL
)

declare @ID int
DECLARE @IDAnagrafica int
DECLARE @IDAzienda int
DECLARE @IDCampoContattoAz int
DECLARE @voce varchar(400)

declare @IDOLD int
DECLARE @IDAnagraficaOLD int

declare @i int

declare @ret int

set @i=0
set @ret=0

DECLARE db_cursor CURSOR FOR 
select
AnagContattiAz.ID,
IDAnagrafica,
AnagContattiAz.IDAzienda,
IDCampoContattoAz,
isnull(Voce,'')

FROM AnagContattiAz
left join Anagrafica on Anagrafica.ID=AnagContattiAz.IDAnagrafica
WHERE IDCampoContattoAz in (3,4)
order by idanagrafica
--and idanagrafica in (157,189)

OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO @iD, @IDAnagrafica, @IDAzienda, @IDCampoContattoAz, @voce

WHILE @@FETCH_STATUS = 0  
BEGIN  

     set @i=@i+1

	 select @ret=count(*) from TempContattiAz where IDAnagrafica=@IDAnagrafica

	 if (@ret=0)
	 begin
	  if (@IDCampoContattoAz=3)
	     begin
			INSERT INTO TempContattiAz (iD,IDAnagrafica,IDAzienda,IDCampoContattoAz,voce1) VALUES (@iD, @IDAnagrafica, @IDAzienda, @IDCampoContattoAz, @voce)
		 end
	  if (@IDCampoContattoAz=4)
	     begin
			INSERT INTO TempContattiAz (iD,IDAnagrafica,IDAzienda,IDCampoContattoAz,voce2) VALUES (@iD, @IDAnagrafica, @IDAzienda, @IDCampoContattoAz, @voce)
		 end
	 end
	 else
	 begin
	  if (@IDCampoContattoAz=3)
	     begin
			UPDATE TempContattiAz SET voce1 = @voce WHERE IDAnagrafica=@IDAnagrafica
		 end
	  if (@IDCampoContattoAz=4)
	     begin
			UPDATE TempContattiAz SET voce2 = @voce WHERE IDAnagrafica=@IDAnagrafica
		 end
	 end

     FETCH NEXT FROM db_cursor INTO @iD, @IDAnagrafica, @IDAzienda, @IDCampoContattoAz, @voce

END
CLOSE db_cursor  
DEALLOCATE db_cursor 


SELECT
'B09'															  																																		AS 'Tipo record',
'C'																  																																		AS 'Modalità operativa',
'000000'														  																																		AS 'Codice azienda',
RIGHT('0000000000000000' + CONVERT(varchar(16), IDAnagrafica),16)																																		AS 'Codice soggetto',
--ISNULL(FORMAT(Anagrafica.DataNascita,'dd/MM/yyyy'),FORMAT((SELECT TOP 1 DallaData FROM StoricoAnagrafica WHERE StoricoAnagrafica.IDAnagrafica = IDAnagrafica ORDER BY DallaData DESC),'dd/MM/yyyy'))   AS 'Data inizio validità',
ISNULL(FORMAT((SELECT TOP 1 DallaData FROM StoricoAnagrafica WHERE StoricoAnagrafica.IDAnagrafica = TempContattiAz.IDAnagrafica ORDER BY DallaData ASC),'dd/MM/yyyy'), FORMAT(Anagrafica.DataNascita,'dd/MM/yyyy')) AS 'Data inizio validità',
'31/12/2999'																																															AS 'Data fine validità',
'RA'														       	  AS 'Tipo indirizzo',
''					 											      AS 'Indirizzo',
''					 											      AS 'N° civico',
''					 											      AS 'Comune',
''					 											      AS 'CAP',
''					 											      AS 'Provincia',
''					 											      AS 'Codice catastale',
''					 											      AS 'Stato',
''					 											      AS 'Prefisso',
''					 											      AS 'Telefono',
''					 											      AS 'Fax',
isnull(voce1,'')													  AS 'Cellulare',
isnull(voce2,'')													  AS 'E-mail',
''																	  AS 'Indirizzo web',
''																	  AS 'Riferimento stanza ufficio'
FROM TempContattiAz
left join Anagrafica on Anagrafica.ID=TempContattiAz.IDAnagrafica
WHERE IDCampoContattoAz in (3,4)
order by idanagrafica


