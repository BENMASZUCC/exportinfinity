/* PRINCIPALE = CANDIDATURA */



/*****************************************************************************************************************/
/**************************************** CANDIDATURA-SETTORE FUNZIONALE*  ***************************************/
/*****************************************************************************************************************/

SELECT
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'SETT_FUNZ'		     												  AS 'Tabella di riferimento',
''																	  AS 'Codice settore funzionale',
''																	  AS 'Codice settore funzionale'


/*****************************************************************************************************************/
/***************************************** CANDIDATURA-TIPO RAPPORTO*  *******************************************/
/*****************************************************************************************************************/

SELECT
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'TIPO_RAPP'		     												  AS 'Tabella di riferimento',
''																	  AS 'Codice rapporto',
''																	  AS 'Codice rapporto'


/*****************************************************************************************************************/
/********************************** CANDIDATURA-DISPONIBILITA TRASFERIMENTI*  ************************************/
/*****************************************************************************************************************/


;SELECT
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'DISP_TRASF'	     												  AS 'Tabella di riferimento',
'0000000000'	     												  AS 'Codice',
'Non disponibile'     												  AS 'Descrizione'
UNION
SELECT
'A02'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'DISP_TRASF'	     												  AS 'Tabella di riferimento',
'0000000001'	     												  AS 'Codice',
'Disponibile'	     												  AS 'Descrizione'


/*****************************************************************************************************************/
/************************************** CANDIDATURA-STATUS CANDIDATURA*  *****************************************/
/*****************************************************************************************************************/

;SELECT
'A02'																		AS 'Tipo record',
'C'																			AS 'Modalità operativa',
'STATUS_TRASF'	     														AS 'Tabella di riferimento',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), ID),10),'')				AS 'Codice',
Stato			     														AS 'Descrizione'
FROM EBC_Stati

/*****************************************************************************************************************/
/************************************** CANDIDATURA-POSIZIONE MILITARE * *****************************************/
/*****************************************************************************************************************/

;SELECT
'A02'																		AS 'Tipo record',
'C'																			AS 'Modalità operativa',
'POSIZ_MIL'	     														AS 'Tabella di riferimento',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), ID),10),'')				AS 'Codice posizione',
Descrizione			     													AS 'Descrizione posizione'
FROM INFINITY_MILITARE


/*****************************************************************************************************************/
/************************************** CANDIDATURA-GIUDIZIONSINTETICO *******************************************/
/*****************************************************************************************************************/

/* ?????????????????????????????????????????????? */


/*****************************************************************************************************************/
/************************************** CANDIDATURA-TIPOLOGIA SELEZIONE ******************************************/
/*****************************************************************************************************************/

;SELECT
'A02'																		AS 'Tipo record',
'C'																			AS 'Modalità operativa',
'TIPO_SELEZ'	     														AS 'Tabella di riferimento',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), ID),10),'')				AS 'Codice posizione',
TipoCommessa			     												AS 'Descrizione posizione'
FROM EBC_TipiCommesse


/*****************************************************************************************************************/
/************************************* CANDIDATURA-CAMPAGNE DI SELEZIONE *****************************************/
/*****************************************************************************************************************/

;SELECT
'D01'																		AS 'Tipo record',
'A'																			AS 'Modalità operativa',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), r.ID),10),'')				AS 'Codice campagna',
isnull(r.ID,'')			     												AS 'Descrizione campagna', /*????*/
isnull(RIGHT('0000000000' + CONVERT(varchar(10), t.ID),10),'')				AS 'Tipologia campagna',
''						     												AS 'Responsabile selezione',
FORMAT(DataInizio,'dd/MM/yyyy')												AS 'Data Inizio',
FORMAT(DataFine,'dd/MM/yyyy')												AS 'Data Fine',
''																			AS 'Numero persone da ricercare',
''																			AS 'Costi previsti',
''																			AS 'Costi sostenuti',
''																			AS 'Note'
  FROM EBC_Ricerche r
  left outer join EBC_TipiCommesse t on r.Tipo=t.TipoCommessa


/*****************************************************************************************************************/
/********************************** CANDIDATURA-TIPOLOGIA ALBO PROFESSIONALE *************************************/
/*****************************************************************************************************************/

;SELECT
'A02'																		AS 'Tipo record',
'C'																			AS 'Modalità operativa',
'TIPO_ALBO'		     														AS 'Tabella di riferimento',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), ID),10),'')				AS 'Codice albo',
Descrizione				     												AS 'Descrizione'
from CatProfess



/*****************************************************************************************************************/
/**************************************** CANDIDATURA-TIPOLOGIA CORSI ********************************************/
/*****************************************************************************************************************/

;SELECT
'A02'																		AS 'Tipo record',
'C'																			AS 'Modalità operativa',
'TIPO_CORSI'																AS 'tabella di riferimento',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), ID),10),'')			 	AS 'Codice tipo corsi',
isnull(Descrizione,'')			 											AS 'Descrizione tipo corsi',
''						     												AS 'Descrizione area tematica'
FROM INFINITY_TIPOLOGIACORSI




/*****************************************************************************************************************/
/*********************** CANDIDATURA-Esperienze lavorative associati alle anagrafiche  *  ************************/
/*****************************************************************************************************************/


;SELECT
'B14'																			AS 'Tipo record',
'C'																				AS 'Modalità operativa',
'000000'																		AS 'Codice azienda',
isnull(RIGHT('0000000000000000' + CONVERT(varchar(16), IDAnagrafica),16),'')	AS 'Codice soggetto',
isnull(RIGHT('00' + CONVERT(varchar(2), MeseDal),2),'')							AS 'Dal mese',
isnull(RIGHT('0000' + CONVERT(varchar(4), AnnoDal),4),'')						AS 'Dal anno',
isnull(RIGHT('00' + CONVERT(varchar(2), MeseAl),2),'')							AS 'Al mese',
isnull(RIGHT('0000' + CONVERT(varchar(4), AnnoAl),4),'')						AS 'Al anno',
isnull(DurataMesi,0)															AS 'Durata',
'MM'																			AS 'Tipo Durata', /*Mesi*/
isnull(aziendaNome,'')															AS 'NomeAzienda',
isnull(REPLACE(CAST(DescrizioneMansione AS NVARCHAR(MAX)), char(10), char(13)),'')   AS 'Maggiori attività svolte',
isnull(REPLACE(CAST(TitoloMansione AS NVARCHAR(MAX)), char(10), char(13)),'')        AS 'Ruolo',
''																					 AS 'Settore merceologico',
''																					 AS 'Settore funzionale',
''																					 AS 'Tipo rapporto',
'EUR'																				 AS 'valuta',
'0'																					 AS 'Retribuzione annua variabile',
isnull(RetribNettaMensile,0)*isnull(Mensilita,0)   								     AS 'Retribuzione annua lorda',
''																					 AS 'CCNL/Livello',
''																					 AS 'Breve descrizione dellazienda',
'000000'																			 AS 'Codice azieda del ruolo'


 FROM EsperienzeLavorative
 left outer join mansioni M on M.ID = EsperienzeLavorative.IDMansione
 where IDAnagrafica is not null
  order by IDAnagrafica
 --select * FROM EsperienzeLavorative  where IDAnagrafica is not null order by IDAnagrafica
-- select * from AnagContratti


/*****************************************************************************************************************/
/*************************** CANDIDATURA-Corsi e stage associati alle anagrafiche  *  ****************************/
/*****************************************************************************************************************/

;SELECT
'B43'																  AS 'Tipo record',
'C'																	  AS 'Modalità operativa',
'000000'		     												  AS 'Codice azienda',
isnull(RIGHT('0000000000000000' + CONVERT(varchar(16), CE.IDAnagrafica),16),'')  AS 'Codice soggetto',
'01'																  AS 'Dal (giorno)',
'01'																  AS 'Dal (mese)',
isnull(ConseguitoAnno,'')											  AS 'Dal (anno)',
'01'																  AS 'Al (giorno)',
'01'																  AS 'Al (mese)',
isnull(ConseguitoAnno,'')											  AS 'Al (anno)',
'0'																	  AS 'Durata',
'HH'																  AS 'Tipo durata',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), tc.ID),10),'')		  AS 'Tipo corso',
''																	  AS 'Area tematica',
''																	  AS 'Ente organizzatore',
''																	  AS 'Nome azienda',
isnull(REPLACE(CAST(ce.Argomenti AS NVARCHAR(MAX)), char(10), char(13)),'') AS 'Argomento/contenuto',
''																	  AS 'Titolo',
''																	  AS 'Note',
CASE
    WHEN ConseguitoAnno is not null THEN '01/01/' + isnull(CAST(ConseguitoAnno AS NVARCHAR(MAX)),'')
    ELSE ''
END																	  AS 'Data rilascio certificato',
'0'																	  AS 'Crediti',
''																	  AS 'Professione',
''																	  AS 'Disciplina'
FROM CorsiExtra ce
inner join INFINITY_TIPOLOGIACORSI tc on tc.Descrizione=ce.tipologia


/*****************************************************************************************************************/
/*********************** CANDIDATURA-Associazione albo professionale alle anagrafiche  ***************************/
/*****************************************************************************************************************/

;SELECT
'B19'																			 AS 'Tipo record',
'A'																				 AS 'Modalità operativa',
'000000'		     															 AS 'Codice azienda',
isnull(RIGHT('0000000000000000' + CONVERT(varchar(16), ac.IDAnagrafica),16),'')  AS 'Codice soggetto',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), ac.idcatprofess),10),'')		 AS 'Tipologia albo',
FORMAT(DataConseg,'dd/MM/yyyy')	 												 AS 'Data iscrizione',
''																				 AS 'Data Chiusura',
''																				 AS 'Provincia iscrizione',
''																				 AS 'Numero iscrizione',
''																				 AS 'Note'
from AnagCatProfess ac


/*****************************************************************************************************************/
/****************************** CANDIDATURA-Candidati associati alle campagnee  **********************************/
/*****************************************************************************************************************/

;SELECT
'D02'																		  AS 'Tipo record',
'A'																			  AS 'Modalità operativa',
isnull(RIGHT('0000000000' + CONVERT(varchar(10), IDRicerca),10),'')		  	  AS 'Codice campagna',
'000000'				     												  AS 'Codice azienda',
isnull(RIGHT('0000000000000000' + CONVERT(varchar(16), IDAnagrafica),16),'')  AS 'Codice soggetto',
FORMAT(DataIns,'dd/MM/yyyy')												  AS 'Data campagna selezione',
'9999999999'																  AS 'Codice richiesta'
FROM EBC_CandidatiRicerche